import { ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import configuration from "src/common/configuration";

import { AppModule } from "./core/modules/app/app.module";

// var fs = require("fs");

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: { origin: [...configuration().ALLOWED_ORIGINS], credentials: true },
  });
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  // const config = new DocumentBuilder()
  //   .setTitle("Offblocks API")
  //   .setDescription("Offblocks API description")
  //   .setVersion("beta")
  //   .addTag("api")
  //   .addBearerAuth()
  //   .build();

  // const document = SwaggerModule.createDocument(app, config);
  // fs.writeFile("openApi.json", JSON.stringify(document), (e) => console.log(e));

  // SwaggerModule.setup("api", app, document);

  await app.listen(process.env.PORT_BACKEND || 8080);
}
bootstrap();
