export default () => {
  const parseStringEnv = (name: string) => {
    const value: string = process.env[name];

    if (!value) {
      throw new Error(`Invalid env ${name}`);
    }

    return value;
  };

  const parseIntEnv = (name: string) => {
    const value: string = process.env[name];

    const int: number = parseInt(value);

    if (isNaN(int)) {
      throw new Error(`Invalid env ${name}`);
    }

    return int;
  };

  const parseBoolEnv = (name: string) => {
    const value: string = process.env[name];

    if (value === "false") {
      return false;
    }

    if (value === "true") {
      return true;
    }

    throw new Error(`Invalid env ${name}`);
  };

  const MONGOURI = parseStringEnv("MONGO_URI");
  // const DBHOST = parseStringEnv("DATABASE_HOST");
  // const DBPORT = parseStringEnv("DATABASE_PORT");
  // const DBUSER = parseStringEnv("DATABASE_USER");
  // const DBPWD = parseStringEnv("DATABASE_PWD");
  // const DBNAME = parseStringEnv("DATABASE_NAME");
  // console.log("Database: ", DBHOST, DBPORT, DBUSER, DBPWD, DBNAME);

  const JWT = parseStringEnv("JWT_SECRET");
  const VERIFICATION_JWT = parseStringEnv("VERIFICATION_JWT_SECRET");
  const PORT = parseIntEnv("PORT_BACKEND");
  const CLIENT_HOST = parseStringEnv("CLIENT_HOST");
  const FRONTURL = parseStringEnv("FRONT_URL");
  // console.log("Server:", JWT, PORT, CLIENT_HOST);

  const SMTPHOST = parseStringEnv("SMTP_HOST");
  const SMTPPORT = parseIntEnv("SMTP_PORT");
  const SMTPUSER = parseStringEnv("SMTP_USER");
  const SMTPPWD = parseStringEnv("SMTP_PWD");
  const SMTPFROM = parseStringEnv("SMTP_FROM");
  // console.log("Email:", SMTPHOST, SMTPPORT, SMTPUSER, SMTPPWD, SMTPFROM);

  return {
    PORT,
    JWT,
    VERIFICATION_JWT,
    MONGO_URI: MONGOURI,
    ALLOWED_ORIGINS: CLIENT_HOST.split(";").map((host: string) => host.trim()),
    FRONTURL,
    SMTP: {
      HOST: SMTPHOST,
      PORT: SMTPPORT,
      USER: SMTPUSER,
      PWD: SMTPPWD,
      FROM: SMTPFROM,
    },
  };
};
