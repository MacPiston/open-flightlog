import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Request,
  UseInterceptors,
  Delete,
} from "@nestjs/common";
import { ApiBearerAuth, ApiParam, ApiTags } from "@nestjs/swagger";
import { SanitizeMongooseModelInterceptor } from "nestjs-mongoose-exclude";

import { JwtAuthGuard } from "../../../guards/jwt-auth.guard";
import { ParseObjectIdPipe } from "../../../pipes/parseObjectId.pipe";
import { FlightDraft } from "../../../schemas/flight-draft/flightDraft.schema";
import { UpdateDraftDto } from "../models/dto/updateDraft.dto";
import { FlightDraftService } from "../service/flight-draft.service";

@Controller("flight-draft")
@ApiTags("flight-draft")
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(
  new SanitizeMongooseModelInterceptor({
    excludeMongooseId: false,
    excludeMongooseV: true,
  })
)
export class FlightDraftController {
  constructor(private readonly flightDraftService: FlightDraftService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get("all")
  async getAll(@Request() req): Promise<FlightDraft[]> {
    const { _id } = req.user;

    return await this.flightDraftService.findAll(_id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  async createDraft(@Request() req): Promise<FlightDraft> {
    const { _id } = req.user;

    return await this.flightDraftService.createDraft(_id);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "draftId" })
  @UseGuards(JwtAuthGuard)
  @Delete(":draftId")
  async delete(
    @Request() req,
    @Param("draftId", ParseObjectIdPipe) draftId: string
  ) {
    const { _id } = req.user;

    return await this.flightDraftService.delete(_id, draftId);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "draftId" })
  @UseGuards(JwtAuthGuard)
  @Put(":draftId")
  async updateDraft(
    @Param("draftId", ParseObjectIdPipe) draftId: string,
    @Request() req,
    @Body() dto: UpdateDraftDto
  ): Promise<FlightDraft> {
    return await this.flightDraftService.updateDraft(draftId, dto);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "draftId" })
  @UseGuards(JwtAuthGuard)
  @Get(":draftId")
  async getDraft(
    @Param("draftId", ParseObjectIdPipe) draftId: string
  ): Promise<FlightDraft> {
    return await this.flightDraftService.getById(draftId);
  }
}
