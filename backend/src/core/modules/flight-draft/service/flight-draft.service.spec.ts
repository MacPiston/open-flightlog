import { Test, TestingModule } from "@nestjs/testing";
import { FlightDraftService } from "./flight-draft.service";

describe("FlightDraftService", () => {
  let service: FlightDraftService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FlightDraftService],
    }).compile();

    service = module.get<FlightDraftService>(FlightDraftService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
