import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { addHours } from "date-fns";
import { Model } from "mongoose";

import {
  FlightDraft,
  FlightDraftDocument,
} from "../../../schemas/flight-draft/flightDraft.schema";
import { UserService } from "../../user/service/user.service";
import { UpdateDraftDto } from "../models/dto/updateDraft.dto";

@Injectable()
export class FlightDraftService {
  constructor(
    @InjectModel(FlightDraft.name)
    private readonly flightDraftModel: Model<FlightDraftDocument>,
    private readonly userService: UserService
  ) {}

  async findAll(userId: string): Promise<FlightDraft[]> {
    const currentUser = await this.userService.getById(userId);

    return await this.flightDraftModel.find({ owner: currentUser });
  }

  async createDraft(userId: string): Promise<FlightDraft> {
    const currentUser = await this.userService.getById(userId);

    return await this.flightDraftModel.create({
      owner: currentUser,
      departureDate: new Date(),
      arrivalDate: addHours(new Date(), 1),
    });
  }

  async getById(draftId: string): Promise<FlightDraft> {
    const currentDraft = await this.flightDraftModel.findById(draftId);
    if (!currentDraft) throw new NotFoundException();

    return currentDraft;
  }

  async delete(userId: string, draftId: string) {
    const currentUser = await this.userService.getById(userId);
    return await this.flightDraftModel.deleteOne({
      _id: draftId,
      owner: currentUser,
    });
  }

  async updateDraft(
    draftId: string,
    dto: UpdateDraftDto
  ): Promise<FlightDraft> {
    const { departureDate, arrivalDate, ...rest } = dto;

    const updatedDraft = await this.flightDraftModel.findByIdAndUpdate(
      draftId,
      {
        $set: {
          departureDate: new Date(departureDate),
          arrivalDate: new Date(arrivalDate),
          ...rest,
        },
      },
      {
        new: true,
      }
    );
    if (!updatedDraft) throw new NotFoundException("Draft not found");

    return updatedDraft;
  }

  //PRIVATE HELPER METHODS
}
