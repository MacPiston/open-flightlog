import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import * as path from "path";
import {
  AcceptLanguageResolver,
  CookieResolver,
  HeaderResolver,
  I18nJsonParser,
  I18nModule,
  QueryResolver,
} from "nestjs-i18n";

import { AppController } from "./controller/app.controller";
import { UserModule } from "../user/user.module";
import configuration from "../../../common/configuration";
import { AuthModule } from "../auth/auth.module";
import { AircraftModule } from "../aircraft/aircraft.module";
import { FlightModule } from "../flight/flight.module";
import { FlightDraftModule } from "../flight-draft/flight-draft.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    I18nModule.forRoot({
      fallbackLanguage: "en",
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, "..", "..", "..", "common", "i18n"),
      },
      resolvers: [
        { use: QueryResolver, options: ["lang", "locale", ""] },
        new HeaderResolver(["x-custom-lang"]),
        AcceptLanguageResolver,
        new CookieResolver(["lang", "locale", "l"]),
      ],
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>("MONGO_URI"),
      }),
    }),
    UserModule,
    AircraftModule,
    FlightModule,
    FlightDraftModule,
    AuthModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
