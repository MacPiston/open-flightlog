import {
  ConflictException,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import * as bcrypt from "bcrypt";

import { User, UserDocument } from "../../../schemas/user/user.schema";
import { CreateUserDto } from "../models/dto/createUser.dto";
import { UpdateUserDto } from "../models/dto/updateUser.dto";
import { MailService } from "../../mail/service/mail.service";
import { AuthService } from "../../auth/service/auth.service";
import { Flight, FlightDocument } from "src/core/schemas/flight/flight.schema";
import {
  Aircraft,
  AircraftDocument,
} from "src/core/schemas/aircraft/aircraft.schema";
import { SummaryResponse } from "src/core/modules/user/models/response/summaryResponse";
import {
  FlightDraft,
  FlightDraftDocument,
} from "src/core/schemas/flight-draft/flightDraft.schema";
import { differenceInMinutes, differenceInMonths } from "date-fns";

@Injectable()
export class UserService {
  private readonly saltOrRounds = 10;
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
    @InjectModel(Flight.name)
    private readonly flightModel: Model<FlightDocument>,
    @InjectModel(Aircraft.name)
    private readonly aircraftModel: Model<AircraftDocument>,
    @InjectModel(FlightDraft.name)
    private readonly flightDraftModel: Model<FlightDraftDocument>,
    private readonly mailService: MailService,
    private readonly authService: AuthService
  ) {}

  async findAll(): Promise<User[] | undefined> {
    return await this.userModel.find();
  }

  async getByUsername(username: string): Promise<User | undefined> {
    const currentUser = await this.userModel.findOne({ username });
    if (!currentUser) throw new NotFoundException("User not found");

    return currentUser;
  }

  async getByEmail(email: string): Promise<User | undefined> {
    const currentUser = await this.userModel.findOne({ email });
    if (!currentUser) throw new NotFoundException("User not found");

    return currentUser;
  }

  async getById(userId: string) {
    const currentUser = await this.userModel.findById(userId);
    if (!currentUser) throw new NotFoundException("User not found");

    return currentUser;
  }

  async createUser(dto: CreateUserDto, lang: string) {
    await this.checkForDuplicate(dto);

    const { email, username, password } = dto;

    const hash = await bcrypt.hash(password, this.saltOrRounds);
    const createdUser = new this.userModel({
      email,
      username,
      password: hash,
    });
    const savedUser = await createdUser.save();

    const token = await this.authService.getEmailVerificationToken(savedUser);
    await this.mailService.sendActivationEmail(savedUser, token, lang);

    return savedUser;
  }

  async updateUser(dto: UpdateUserDto & { _id: string }) {
    const { username, licenceNumber, _id } = dto;
    return await this.userModel.updateOne(
      { id: _id },
      { username, licenceNumber }
    );
  }

  async getSummary(userId: string): Promise<SummaryResponse> {
    const currentUser = await this.getById(userId);

    const aircraftsNumber = await this.aircraftModel.countDocuments({
      owner: currentUser,
    });

    const flightsNumber = await this.flightModel.countDocuments({
      owner: currentUser,
    });

    const latestFlightDraft = await this.flightDraftModel
      .findOne({ owner: currentUser })
      .sort("-createdOn");
    const latestDraftId = latestFlightDraft
      ? latestFlightDraft._id.toString()
      : undefined;

    const latestFlight = await this.flightModel
      .findOne({ owner: currentUser })
      .sort("-createdOn");
    const latestFlightId = latestFlight
      ? latestFlight._id.toString()
      : undefined;

    const flights = await this.flightModel.find({ owner: currentUser });
    const currentDate = new Date();
    let totalMinutes = 0;
    let currentMonthMinutes = 0;
    flights.forEach((flight) => {
      const duration = differenceInMinutes(
        flight.arrivalDate,
        flight.departureDate
      );
      totalMinutes += duration;
      if (differenceInMonths(currentDate, flight.departureDate) < 1) {
        currentMonthMinutes += duration;
      }
    });

    return {
      aircraftsNumber,
      flightsNumber,
      latestDraftId,
      latestFlightId,
      totalMinutes,
      currentMonthMinutes,
    };
  }

  // PRIVATE HELPER METHODS

  private async checkForDuplicate(dto: CreateUserDto) {
    if (await this.userModel.findOne({ username: dto.username }))
      throw new ConflictException();
    if (await this.userModel.findOne({ email: dto.email }))
      throw new ConflictException();
  }
}
