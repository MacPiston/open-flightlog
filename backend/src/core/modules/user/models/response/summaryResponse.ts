export class SummaryResponse {
  aircraftsNumber: number;
  flightsNumber: number;
  latestDraftId: string;
  latestFlightId: string;
  currentMonthMinutes: number;
  totalMinutes: number;
}
