export class TokenResponse {
  token: string;
  email: string;
  username: string;
}
