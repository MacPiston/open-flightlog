import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigModule } from "@nestjs/config";

import { UserService } from "./service/user.service";
import { UserController } from "./controller/user.controller";
import { User, UserSchema } from "../../schemas/user/user.schema";
import { JwtStrategy } from "../../strategies/jwt.strategy";
import { MailModule } from "../mail/mail.module";
import { AuthModule } from "../auth/auth.module";
import {
  Aircraft,
  AircraftSchema,
} from "src/core/schemas/aircraft/aircraft.schema";
import { Flight, FlightSchema } from "src/core/schemas/flight/flight.schema";
import {
  FlightDraft,
  FlightDraftSchema,
} from "src/core/schemas/flight-draft/flightDraft.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Aircraft.name, schema: AircraftSchema },
      { name: Flight.name, schema: FlightSchema },
      { name: FlightDraft.name, schema: FlightDraftSchema },
    ]),
    MailModule,
    ConfigModule,
    AuthModule,
  ],
  providers: [UserService, JwtStrategy],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
