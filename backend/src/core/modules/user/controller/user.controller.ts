import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  Request,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { I18nLang } from "nestjs-i18n";

import { JwtAuthGuard } from "../../../guards/jwt-auth.guard";
import MongooseClassSerializerInterceptor from "../../../interceptors/mongooseClassSerializer.interceptor";

import { User } from "../../../schemas/user/user.schema";
import { CreateUserDto } from "../models/dto/createUser.dto";
import { UpdateUserDto } from "../models/dto/updateUser.dto";
import { UserService } from "../service/user.service";

@Controller("user")
@ApiTags("user")
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(MongooseClassSerializerInterceptor(User))
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async createUser(@Body() dto: CreateUserDto, @I18nLang() lang: string) {
    return await this.userService.createUser(dto, lang);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get("profile")
  async getProfile(@Request() req) {
    const { _id } = req.user;
    return await this.userService.getById(_id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put("profile")
  async updateProfile(@Request() req, @Body() dto: UpdateUserDto) {
    const { _id } = req.user;
    return await this.userService.updateUser({ _id, ...dto });
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get("summary")
  async getSummary(@Request() req) {
    const { _id } = req.user;
    return await this.userService.getSummary(_id);
  }
}
