import {
  Body,
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Request,
} from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { JwtAuthGuard } from "src/core/guards/jwt-auth.guard";
import { ConfirmDto } from "src/core/modules/auth/models/dto/confirm.dto";
import { LoginDto } from "src/core/modules/auth/models/dto/login.dto";

import { AuthService } from "../service/auth.service";

@Controller("auth")
@ApiTags("auth")
@UsePipes(new ValidationPipe({ transform: true }))
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("login")
  async login(@Body() dto: LoginDto) {
    const { email, password } = dto;
    return await this.authService.login(email, password);
  }

  @Post("confirm")
  async confirmEmail(@Body() dto: ConfirmDto) {
    return await this.authService.verifyEmail(dto.token);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post("refresh")
  async refreshToken(@Request() req) {
    const { _id } = req.user;
    return await this.authService.refreshToken(_id);
  }
}
