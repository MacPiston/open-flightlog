export class JwtPayload {
  username: string;
  _id: string;
}
