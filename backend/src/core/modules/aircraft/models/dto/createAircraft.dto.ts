import { IsNotEmpty, IsOptional, IsString, MaxLength } from "class-validator";

export class CreateAircraftDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(10)
  registration: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(15)
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(10)
  type: string;
}
