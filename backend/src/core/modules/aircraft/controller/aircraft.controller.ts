import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from "@nestjs/common";
import { ApiBearerAuth, ApiParam, ApiTags } from "@nestjs/swagger";

import { JwtAuthGuard } from "../../../guards/jwt-auth.guard";
import MongooseClassSerializerInterceptor from "../../../interceptors/mongooseClassSerializer.interceptor";
import { ParseObjectIdPipe } from "../../../pipes/parseObjectId.pipe";
import { Aircraft } from "../../../schemas/aircraft/aircraft.schema";
import { CreateAircraftDto } from "../models/dto/createAircraft.dto";
import { UpdateAircraftDto } from "../models/dto/updateAircraft.dto";
import { AircraftService } from "../service/aircraft.service";

@Controller("aircraft")
@ApiTags("aircraft")
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(MongooseClassSerializerInterceptor(Aircraft))
export class AircraftController {
  constructor(private readonly aircraftService: AircraftService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get("all")
  async getAll(@Request() req) {
    const { _id } = req.user;

    return await this.aircraftService.findAll(_id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  async createAircraft(@Request() req, @Body() dto: CreateAircraftDto) {
    const { _id } = req.user;

    return await this.aircraftService.createAircraft(_id, dto);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "aircraftId" })
  @UseGuards(JwtAuthGuard)
  @Get(":aircraftId")
  async getAircraft(
    @Request() req,
    @Param("aircraftId", ParseObjectIdPipe) aircraftId: string
  ) {
    const { _id } = req.user;

    return await this.aircraftService.findById(_id, aircraftId);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "aircraftId" })
  @UseGuards(JwtAuthGuard)
  @Put(":aircraftId")
  async updateAircraft(
    @Param("aircraftId", ParseObjectIdPipe) aircraftId: string,
    @Body() dto: UpdateAircraftDto
  ) {
    return await this.aircraftService.updateAircraft(aircraftId, dto);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "aircraftId" })
  @UseGuards(JwtAuthGuard)
  @Delete(":aircraftId")
  async deleteAircraft(
    @Request() req,
    @Param("aircraftId", ParseObjectIdPipe) aircraftId: string
  ) {
    const { _id } = req.user;

    return await this.aircraftService.deleteAircraft(_id, aircraftId);
  }
}
