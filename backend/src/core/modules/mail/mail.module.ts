import { Module } from "@nestjs/common";
import { MailService } from "./service/mail.service";
import { MailerModule } from "@nestjs-modules/mailer";
import { HandlebarsAdapter } from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";
import { ConfigModule, ConfigService } from "@nestjs/config";
import * as path from "path";
import { I18nModule } from "nestjs-i18n";

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          transport: {
            host: configService.get<string>("SMTP.HOST"),
            port: configService.get<number>("SMTP.PORT"),
            secure: false,
            auth: {
              user: configService.get<string>("SMTP.USER"),
              pass: configService.get<string>("SMTP.PWD"),
            },
          },
          template: {
            dir: path.join(__dirname, "templates"),
            adapter: new HandlebarsAdapter(),
            options: {
              strict: true,
            },
          },
        };
      },
    }),
    ConfigModule,
    I18nModule,
  ],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
