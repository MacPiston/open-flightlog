import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Exclude, Transform } from "class-transformer";
import * as mongoose from "mongoose";
import { Document } from "mongoose";

import { Aircraft } from "../aircraft/aircraft.schema";

export type UserDocument = User & Document;

@Schema()
export class User {
  @Transform(({ value }) => value.toString())
  _id: string;

  @Prop({ required: true, unique: true })
  username: string;

  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  @Exclude()
  password: string;

  @Prop()
  licenceNumber?: string;

  @Prop()
  avatarUrl?: string;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: "Aircraft" }] })
  @Exclude()
  aircrafts: Aircraft[];

  @Prop({ default: false })
  emailVerified: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
