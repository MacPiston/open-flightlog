import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Exclude } from "class-transformer";
import * as mongoose from "mongoose";
import { Document } from "mongoose";

import { Flight } from "../flight/flight.schema";
import { User } from "../user/user.schema";

export type AircraftDocument = Aircraft & Document;

@Schema()
export class Aircraft {
  @Prop()
  registration: string;

  @Prop()
  name: string;

  @Prop()
  type: string;

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: "Flight" }],
    default: [],
  })
  flights: Flight[];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: "User" })
  @Exclude()
  owner: User;
}

export const AircraftSchema = SchemaFactory.createForClass(Aircraft);
