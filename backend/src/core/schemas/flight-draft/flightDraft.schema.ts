import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoose from "mongoose";
import { ExcludeProperty } from "nestjs-mongoose-exclude";

import { User } from "../user/user.schema";

import { PathPoint, PathPointSchema } from "../flight/subschemas/pathpoint";
import { PilotTime, PilotTimeSchema } from "../flight/subschemas/pilotTime";
import { Landings, LandingsSchema } from "../flight/subschemas/landings";
import {
  OperationalConditionTime,
  OperationalConditionTimeSchema,
} from "../flight/subschemas/operationalConditionTime";
import {
  PilotFunctionTime,
  PilotFunctionTimeSchema,
} from "../flight/subschemas/pilotFunctionTime";
import {
  SyntheticTrainingDeviceSessionSchema,
  SyntheticTrainingDeviceSession,
} from "../flight/subschemas/synteticTrainingDeviceSession";

export type FlightDraftDocument = FlightDraft & Document;

@Schema()
export class FlightDraft {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  })
  @ExcludeProperty()
  owner: User;

  @Prop({ type: Date, default: new Date() })
  createdOn: Date;

  @Prop({ type: Boolean, default: false })
  @ExcludeProperty()
  flightCreated: boolean;

  @Prop({ type: Date, default: new Date() })
  departureDate: Date;

  @Prop({ type: PathPointSchema, default: {} })
  departurePoint: PathPoint;

  @Prop({ type: [PathPointSchema], default: [] })
  pathPoints: PathPoint[];

  @Prop({ type: Date, default: new Date() })
  arrivalDate: Date;

  @Prop({ type: PathPointSchema, default: {} })
  arrivalPoint: PathPoint;

  @Prop({ type: String, default: "" })
  aircraftId: string;

  @Prop()
  pic?: string;

  @Prop({ type: LandingsSchema, default: {} })
  landings: Landings;

  @Prop({ type: PilotTimeSchema, default: {} })
  pilotTime: PilotTime;

  @Prop({ type: PilotFunctionTimeSchema, default: {} })
  pft: PilotFunctionTime;

  @Prop({ type: OperationalConditionTimeSchema, default: {} })
  oct: OperationalConditionTime;

  @Prop({ type: Number, default: 0 })
  ifrApproaches: number;

  @Prop({ type: SyntheticTrainingDeviceSessionSchema, default: {} })
  stds: SyntheticTrainingDeviceSession;

  @Prop({ type: String })
  remarks: string;
}

export const FlightDraftSchema = SchemaFactory.createForClass(FlightDraft);
