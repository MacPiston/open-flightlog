import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

import {
  IsArray,
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
  ValidateNested,
} from "class-validator";

export enum PointType {
  ARPT = "ARPT",
  WAYP = "WAYP",
}

class GeoJsonPoint {
  @IsArray()
  @MaxLength(2, { each: true })
  coordinates: number[];

  type = "Point";
}

@Schema()
export class PathPoint {
  @Prop()
  @IsOptional()
  @IsString()
  name?: string;

  @Prop()
  @IsOptional()
  @IsString()
  icaoName?: string;

  @Prop({ type: String, enum: ["ARPT", "WAYP"] })
  @IsEnum(PointType)
  type: PointType;

  @Prop()
  @IsOptional()
  @IsString()
  description?: string;

  @Prop()
  @IsOptional()
  @ValidateNested()
  point?: GeoJsonPoint;
}

export const PathPointSchema = SchemaFactory.createForClass(PathPoint);
