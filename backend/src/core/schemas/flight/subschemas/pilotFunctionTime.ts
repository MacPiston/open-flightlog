import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsInt, IsOptional, Min } from "class-validator";

@Schema()
export class PilotFunctionTime {
  @Prop()
  @IsOptional()
  @IsInt()
  @Min(0)
  pic?: number;

  @Prop()
  @IsOptional()
  @IsInt()
  @Min(0)
  coPilot?: number;

  @Prop()
  @IsOptional()
  @IsInt()
  @Min(0)
  dual?: number;

  @Prop()
  @IsOptional()
  @IsInt()
  @Min(0)
  instructor?: number;
}

export const PilotFunctionTimeSchema =
  SchemaFactory.createForClass(PilotFunctionTime);
