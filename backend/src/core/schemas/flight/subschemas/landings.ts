import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsInt, IsOptional, Min } from "class-validator";

@Schema()
export class Landings {
  @Prop({ type: Number, default: 0 })
  @IsOptional()
  @IsInt()
  @Min(0)
  daytime?: number;

  @Prop({ type: Number, default: 0 })
  @IsOptional()
  @IsInt()
  @Min(0)
  nighttime?: number;
}

export const LandingsSchema = SchemaFactory.createForClass(Landings);
