import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsDateString, IsInt, IsNotEmpty, IsString } from "class-validator";

@Schema()
export class SyntheticTrainingDeviceSession {
  @Prop({ type: Date })
  @IsDateString()
  date: Date;

  @Prop()
  @IsString()
  @IsNotEmpty()
  type: string;

  @Prop()
  @IsString()
  @IsInt()
  totalTime: number;
}

export const SyntheticTrainingDeviceSessionSchema =
  SchemaFactory.createForClass(SyntheticTrainingDeviceSession);
