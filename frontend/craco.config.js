const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const CracoAlias = require("craco-alias");

module.exports = {
  webpack: {
    configure: (config, { env, paths }) => {
      config.resolve.plugins.push(new TsconfigPathsPlugin());
      if (process.argv.includes("--analyze")) {
        config.plugins.push(new BundleAnalyzerPlugin());
      }
      return config;
    },
  },
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: "tsconfig",
        // baseUrl SHOULD be specified
        // plugin does not take it from tsconfig
        baseUrl: "./",
        // tsConfigPath should point to the file where "baseUrl" and "paths" are specified
        tsConfigPath: "./tsconfig.extend.json",
      },
    },
  ],
};
