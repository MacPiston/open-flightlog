export const API_URL = String(process.env.REACT_APP_API_URL);

export const LOGIN_ENDPOINT = `${API_URL}/auth/login`;
export const ACTIVATE_ENDPOINT = `${API_URL}/auth/confirm`;
export const REFRESH_ENDPOINT = `${API_URL}/auth/refresh`;
export const USER_ENDPOINT = `${API_URL}/user`;
export const AIRCRAFT_ENDPOINT = `${API_URL}/aircraft`;
export const FLIGHT_DRAFT_ENDPOINT = `${API_URL}/flight-draft`;
export const FLIGHT_ENDPOINT = `${API_URL}/flight`;
