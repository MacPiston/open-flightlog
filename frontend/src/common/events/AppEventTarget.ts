class AppEventTarget extends EventTarget {}

export default new AppEventTarget();

export const appEventTypes = {
  TOKEN_CHANGE: "TOKEN_CHANGE",
  LOGOUT_REQUEST: "LOGOUT_REQUEST",
};
