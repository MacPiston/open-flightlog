import axios from "axios";

import {
  getUserToken,
  setUserToken,
  UserToken,
  userTokenFromResponse,
} from "@src/common/helpers/userToken";
import { REFRESH_ENDPOINT } from "@src/config/baseUrls";
import AppEventTarget from "@src/common/events/AppEventTarget";
import TokenChangeEvent from "@src/common/events/TokenChangeEvent";
import LogoutRequestEvent from "@src/common/events/LogoutRequestEvent";

interface TokenResponse {
  token: string;
  username: string;
  email: string;
}

const useRefreshToken = () => async (userToken: UserToken | null = null) => {
  if (!userToken) userToken = getUserToken();

  const response = await axios.post<TokenResponse>(
    REFRESH_ENDPOINT,
    undefined,
    {
      headers: { Authorization: `Bearer ${userToken?.token}` },
    },
  );

  if (response.status === 200) {
    userToken = userTokenFromResponse(response.data);
    setUserToken(userToken);
    AppEventTarget.dispatchEvent(new TokenChangeEvent(userToken));
  } else {
    AppEventTarget.dispatchEvent(new LogoutRequestEvent());
  }
};

export default useRefreshToken;
