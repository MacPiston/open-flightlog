import { useEffect } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";

import AppEventTarget, {
  appEventTypes,
} from "@src/common/events/AppEventTarget";
import LogoutRequestEvent from "@src/common/events/LogoutRequestEvent";
import { deleteUserToken } from "@src/common/helpers/userToken";
import routesPaths from "@src/modules/Routing/routesPaths";

const useLogoutRequestHandler = () => {
  const navigate = useNavigate();
  //   const axios = useAxios();

  useEffect(() => {
    const handler = async ({
      detail: { showToast, redirectPathname },
    }: LogoutRequestEvent) => {
      // const token = getUserToken();
      //   if (token && getTimeToExpire(token) > 0) {
      //     try {
      //       await axios.post(REVOKE_TOKEN_ENDPOINT);
      //     } catch (error) {
      //       console.error(error);
      //     }
      //   }
      deleteUserToken();

      const target: string = redirectPathname || routesPaths.LANDING;
      //   if (redirectPathname) {
      //     target = `${target}?${new URLSearchParams({
      //       [redirectToQueryParam]: redirectPathname,
      //     }).toString()}`;
      //   }
      navigate(target);

      if (showToast) {
        toast.success("Successfully logged out");
      }
    };

    AppEventTarget.addEventListener(appEventTypes.LOGOUT_REQUEST, {
      handleEvent: handler,
    });
    return () => {
      AppEventTarget.removeEventListener(appEventTypes.LOGOUT_REQUEST, {
        handleEvent: handler,
      });
    };
  }, [navigate]);
};

export default useLogoutRequestHandler;
