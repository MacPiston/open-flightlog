export interface SubRouteProps {
  label: string;
  path: string;
  importComponent: () => Promise<{
    default: React.ComponentType | React.ComponentType<unknown>;
  }>;
}
