import { useNavigate } from "react-router-dom";

import routesPaths from "@src/modules/Routing/routesPaths";
import { useCallback } from "react";

const useRedirects = () => {
  const navigate = useNavigate();

  const toHome = useCallback(() => navigate(routesPaths.HOME), [navigate]);

  const toRegister = useCallback(() => navigate(routesPaths.REGISTER), [
    navigate,
  ]);

  const toFlights = useCallback(
    () =>
      navigate(
        `${routesPaths.flightsPaths.MAIN}${routesPaths.flightsPaths.LIST}`,
      ),
    [navigate],
  );

  const toFlight = useCallback(
    (flightId: string) =>
      navigate(`${routesPaths.flightsPaths.MAIN}/${flightId}`),
    [navigate],
  );

  const toDraft = useCallback(
    (draftId: string) =>
      navigate(`${routesPaths.flightsPaths.MAIN}/drafts/${draftId}`),
    [navigate],
  );

  const toDrafts = useCallback(
    () =>
      navigate(
        `${routesPaths.flightsPaths.MAIN}${routesPaths.flightsPaths.DRAFTS_LIST}`,
      ),
    [navigate],
  );

  const toAircrafts = useCallback(
    () => navigate(routesPaths.aircraftsPaths.MAIN),
    [navigate],
  );

  const toAircraft = useCallback(
    (aircraftId: string) =>
      navigate(`${routesPaths.aircraftsPaths.MAIN}${aircraftId}`),
    [navigate],
  );

  const toNewAircraft = useCallback(
    () =>
      navigate(
        `${routesPaths.aircraftsPaths.MAIN}${routesPaths.aircraftsPaths.NEW}`,
      ),
    [navigate],
  );

  const toLanding = useCallback(() => navigate(routesPaths.LANDING), [
    navigate,
  ]);

  return {
    toHome,
    toRegister,
    toFlights,
    toFlight,
    toDraft,
    toDrafts,
    toAircrafts,
    toAircraft,
    toNewAircraft,
    toLanding,
  };
};

export default useRedirects;
