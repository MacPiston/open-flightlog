import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

import useAxios from "@src/common/axios/useAxios";
import { FLIGHT_DRAFT_ENDPOINT } from "@src/config/baseUrls";

interface NewDraftIdModel {
  _id: string;
}

const useNewDraftId = () => {
  const axios = useAxios();

  return useMutation<AxiosResponse<NewDraftIdModel>, AxiosError>(
    async () => await axios.post(FLIGHT_DRAFT_ENDPOINT),
  );
};

export default useNewDraftId;
