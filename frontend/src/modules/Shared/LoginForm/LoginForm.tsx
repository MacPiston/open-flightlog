import React, { useCallback } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router";

import { LoginFormBackground } from "@src/modules/Shared/LoginForm/LoginFormContent.components";
import useLogin, { LoginModel } from "@src/modules/Shared/LoginForm/useLogin";
import {
  setUserToken,
  userTokenFromResponse,
} from "@src/common/helpers/userToken";
import LoginFormContent from "@src/modules/Shared/LoginForm/LoginFormContent";
import routesPaths from "@src/modules/Routing/routesPaths";

import useNotifications from "@src/modules/Shared/hooks/useNotifications";

interface LoginFormProps {
  title?: string;
}

const initialValues: LoginModel = {
  email: "",
  password: "",
};

const LoginForm: React.FunctionComponent<LoginFormProps> = ({ title }) => {
  const mutation = useLogin();
  const navigate = useNavigate();
  const { error } = useNotifications();

  const validationSchema: Yup.SchemaOf<LoginModel> = Yup.object().shape({
    email: Yup.string().email("Invalid e-mail").required("E-mail required"),
    password: Yup.string()
      .min(6, "Too short!")
      .max(100, "Too long!")
      .required("Password required"),
  });

  const handleSubmit = useCallback(
    async (values: LoginModel) => {
      mutation.mutate(values, {
        onSuccess: (response) => {
          setUserToken(userTokenFromResponse(response.data));
          navigate(routesPaths.HOME);
        },
        onError: (err) => {
          if (err.response?.status === 404) error("Wrong email/password");
        },
      });
    },
    [mutation, navigate, error],
  );

  return (
    <LoginFormBackground>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(formProps) => <LoginFormContent {...formProps} title={title} />}
      </Formik>
    </LoginFormBackground>
  );
};

export default LoginForm;
