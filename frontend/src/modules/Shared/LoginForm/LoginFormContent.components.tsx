import { Box } from "@mui/system";
import { Paper, styled, Typography } from "@mui/material";
import { Form } from "formik";

export const LoginFormBackground = styled(Paper)`
  ${({ theme }) => theme.breakpoints.down("sm")} {
    width: 100%;
  }

  padding: ${({ theme }) => theme.spacing(2)};
  width: 420px;
`;

export const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const LoginFormHeader = styled(Typography)`
  margin-bottom: ${({ theme }) => theme.spacing(1)};
`;

export const TextFieldsWrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  width: 100%;

  & > * {
    :not(:first-child) {
      margin-top: ${({ theme }) => theme.spacing(1)};
    }
  }
`;

export const HorizontalItems = styled(Box)`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  & > :nth-child(2) {
    margin-left: ${({ theme }) => theme.spacing(2)};
  }

  margin-top: ${({ theme }) => theme.spacing(2)};
`;
