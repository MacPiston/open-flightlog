import { styled } from "@mui/material";

import { ReactComponent as OffblocksLogo } from "@src/common/svg/offblocks_logo.svg";

export const StyledLogo = styled(OffblocksLogo)`
  max-width: 180px;
`;
