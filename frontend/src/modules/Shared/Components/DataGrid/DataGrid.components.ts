import styled from "styled-components";
import { AgGridReact } from "ag-grid-react";

export const StyledAgGrid = styled(AgGridReact)`
  position: relative;
  flex: 1;

  .ag-root-wrapper {
    position: absolute;
    inset: 0;
  }
`;
