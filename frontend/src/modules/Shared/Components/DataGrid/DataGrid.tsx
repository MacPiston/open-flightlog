import {
  AgGridColumn,
  AgGridReact,
  AgGridReactProps,
  AgReactUiProps,
} from "ag-grid-react";
import React from "react";

import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

import { StyledAgGrid } from "@src/modules/Shared/components/DataGrid/DataGrid.components";
import {
  defaultColDef,
  ExtendedColDef,
} from "@src/modules/Shared/components/DataGrid/defaults";

interface Props extends AgGridReactProps, AgReactUiProps {
  rows: unknown[] | null | undefined;
  columns: ExtendedColDef[];
  ref?: React.Ref<AgGridReact>;
}

const DataGrid: React.FunctionComponent<Props> = ({
  rows,
  columns,
  ref,
  ...rest
}) => (
  <StyledAgGrid
    className="ag-theme-alpine"
    ref={ref}
    rowData={rows}
    defaultColDef={defaultColDef}
    {...rest}
  >
    {columns.map((c) => {
      const { nestedCols, ...rest } = c;

      return nestedCols ? (
        <AgGridColumn key={rest.headerName} {...rest}>
          {nestedCols.map((nc) => (
            <AgGridColumn key={nc.field} {...nc} />
          ))}
        </AgGridColumn>
      ) : (
        <AgGridColumn key={rest.field} {...rest} />
      );
    })}
  </StyledAgGrid>
);

export default DataGrid;
