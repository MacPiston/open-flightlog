import { ColDef } from "ag-grid-community";

export interface ExtendedColDef extends ColDef {
  nestedCols?: ColDef[];
}

export const defaultColDef: ExtendedColDef = {
  width: 120,
  editable: true,
  resizable: true,
  filter: "agTextColumnFilter",
};
