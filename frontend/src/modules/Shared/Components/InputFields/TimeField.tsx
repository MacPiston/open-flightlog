import React from "react";
import MaskedInput from "react-text-mask";
import { FieldProps, getIn } from "formik";

import { TextField } from "@mui/material";
import {
  StyledTypography,
  TextFieldWrapper,
} from "@src/modules/Shared/components/InputFields/InputFields.components";

const TimeField: React.FunctionComponent<
  FieldProps & { title?: string; error?: boolean; helperText: React.ReactNode }
> = (props) => {
  const { field, form, error, helperText } = props;
  const isTouched = getIn(form.touched, field.name);
  const errorMessage = getIn(form.errors, field.name);

  return (
    <MaskedInput
      {...field}
      showMask
      mask={[/\d/, /\d/, ":", /\d/, /\d/]}
      render={(innerRef, innerProps) => (
        <TextFieldWrapper inputTextCentered>
          {props.title && (
            <StyledTypography variant="overline">
              {props.title}
            </StyledTypography>
          )}
          <TextField
            error={error ?? Boolean(isTouched && errorMessage)}
            helperText={
              helperText ?? (isTouched && errorMessage ? errorMessage : " ")
            }
            variant="outlined"
            size="small"
            inputRef={innerRef}
            {...innerProps}
          />
        </TextFieldWrapper>
      )}
    />
  );
};

export default TimeField;
