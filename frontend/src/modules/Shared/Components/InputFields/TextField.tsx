import React from "react";
import { FieldProps, getIn } from "formik";

import {
  TextFieldWrapper,
  StyledTypography,
  StyledTextField,
} from "@src/modules/Shared/components/InputFields/InputFields.components";
import { InputBaseComponentProps, TextFieldProps } from "@mui/material";

const TextField: React.FunctionComponent<
  TextFieldProps &
    FieldProps & {
      title?: string;
      inputComponent?: React.FunctionComponent<InputBaseComponentProps>;
    }
> = (props) => {
  const { error, helperText, field, form, inputComponent, ...rest } = props;
  const isTouched = getIn(form.touched, field.name);
  const errorMessage = getIn(form.errors, field.name);

  return (
    <TextFieldWrapper>
      {props.title && (
        <StyledTypography variant="overline">{props.title}</StyledTypography>
      )}
      <StyledTextField
        error={error ?? Boolean(isTouched && errorMessage)}
        helperText={
          helperText ?? (isTouched && errorMessage ? errorMessage : " ")
        }
        InputProps={{
          inputComponent,
        }}
        {...rest}
        {...field}
      />
    </TextFieldWrapper>
  );
};

export default TextField;
