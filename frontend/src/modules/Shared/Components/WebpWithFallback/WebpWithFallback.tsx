import React from "react";

interface Props {
  src: string;
  fallback: string;
}

const WebpWithFallback: React.FunctionComponent<Props> = ({
  src,
  fallback,
}) => (
  <div>
    <picture>
      <source srcSet={src} type="image/webp" />
      <img src={fallback} />
    </picture>
  </div>
);

export default WebpWithFallback;
