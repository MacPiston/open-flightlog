import styled from "styled-components";

import { Fab } from "@mui/material";

const ColoredFab = styled(Fab)<{ offset?: number }>`
  position: absolute;
  ${({ offset }) =>
    offset
      ? `bottom: ${offset}px; right: ${offset}px;`
      : "bottom: 0; right: 0;"}
  background-color: ${({ theme }) => theme.palette.success.main};
  color: ${({ theme }) => theme.palette.success.contrastText};

  &:hover {
    background-color: ${({ theme }) => theme.palette.success.light};
  }
`;

export default ColoredFab;
