import { styled, CircularProgress, Backdrop } from "@mui/material";

export const StyledBackdrop = styled(Backdrop)`
  z-index: ${({ theme }) => theme.zIndex.drawer + 1};
`;

export const StyledCircularProgress = styled(CircularProgress)`
  color: ${({ theme }) => theme.palette.primary.main};
`;
