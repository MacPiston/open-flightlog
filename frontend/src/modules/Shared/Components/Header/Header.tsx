import React from "react";

import {
  HeaderWrapper,
  StyledDivider,
  StyledButton,
} from "@src/modules/Shared/components/Header/Header.components";
import { H4 } from "@src/modules/Shared/components/Typography";

interface Props {
  title?: string | null;
  titleComponent?: React.ReactNode;
  icon?: React.ReactNode;
  iconColor?: string;
  iconOnClick?: () => void;
  buttonType?: "button" | "submit" | "reset" | undefined;
}

const Header: React.FunctionComponent<Props> = ({
  title,
  titleComponent,
  icon,
  iconColor,
  iconOnClick,
  buttonType,
}) => (
  <>
    <HeaderWrapper>
      {!titleComponent && title && <H4>{title}</H4>}
      {!title && titleComponent}
      {icon && (
        <StyledButton
          $iconColor={iconColor}
          type={buttonType}
          onClick={iconOnClick}
        >
          {icon}
        </StyledButton>
      )}
    </HeaderWrapper>
    <StyledDivider />
  </>
);

export default Header;
