import styled from "styled-components";

import { Typography } from "@mui/material";

export const Sub1Bold = styled(Typography).attrs({ variant: "subtitle1" })`
  font-weight: ${({ theme }) => theme.typography.fontWeightMedium};
`;

export const Sub1Standard = styled(Typography).attrs({ variant: "subtitle1" })`
  font-weight: ${({ theme }) => theme.typography.fontWeightRegular};
`;

export const H1 = styled(Typography).attrs({ variant: "h1" })``;

export const H2 = styled(Typography).attrs({ variant: "h2" })``;

export const H3 = styled(Typography).attrs({ variant: "h3" })``;

export const H4 = styled(Typography).attrs({ variant: "h4" })``;

export const H5 = styled(Typography).attrs({ variant: "h5" })``;

export const H6 = styled(Typography).attrs({ variant: "h6" })``;
