import { FlightResponse } from "@src/modules/Flights/Components/Flights/hooks/useFlights";
import FlightRow from "@src/modules/Flights/Components/Flights/dataGridConfig/row";
import {
  dateFromISOString,
  minutesToTimeString,
} from "@src/common/helpers/date";

const useParseFlightsData = (
  data?: FlightResponse[],
): FlightRow[] | undefined => {
  if (!data) return undefined;

  return data.map(
    ({
      _id,
      departureDate,
      departurePoint,
      arrivalDate,
      arrivalPoint,
      aircraft,
      pilotTime,
      pic,
      landings,
      pft,
      oct,
      stds,
      ifrApproaches,
      remarks,
    }) => ({
      id: _id,
      departureDate: dateFromISOString(departureDate).toLocaleDateString(),
      departurePlace: departurePoint.icaoName,
      arrivalDate: dateFromISOString(arrivalDate).toLocaleDateString(),
      arrivalPlace: arrivalPoint.icaoName,

      registration: aircraft.registration,
      type: aircraft.type,
      pic,

      landings_total: (
        (landings.daytime || 0) + (landings.nighttime || 0)
      ).toString(),
      landings_day: landings.daytime?.toString() || "0",
      landings_night: landings.nighttime?.toString() || "0",

      pilotTime_SP_total: minutesToTimeString(
        (pilotTime.spse || 0) + (pilotTime.spme || 0),
      ),
      pilotTime_SE: minutesToTimeString(pilotTime.spse) || "-",
      pilotTime_ME: minutesToTimeString(pilotTime.spme) || "-",
      pilotTime_MP: minutesToTimeString(pilotTime.mp) || "-",

      pft_total: minutesToTimeString(
        (pft.pic || 0) +
          (pft.coPilot || 0) +
          (pft.dual || 0) +
          (pft.instructor || 0),
      ),
      pft_pic: minutesToTimeString(pft.pic) || "-",
      pft_copilot: minutesToTimeString(pft.coPilot) || "-",
      pft_dual: minutesToTimeString(pft.dual) || "-",
      pft_instructor: minutesToTimeString(pft.instructor) || "-",

      oct_total: minutesToTimeString((oct.night || 0) + (oct.ifr || 0)) || "-",
      oct_night: minutesToTimeString(oct.night) || "-",
      oct_ifr: minutesToTimeString(oct.ifr) || "-",

      ifrApproaches: ifrApproaches.toString(),

      stds_date: dateFromISOString(stds?.date).toLocaleDateString(),
      stds_time: minutesToTimeString(stds?.totalTime) || "-",
      stds_type: stds?.type || "-",

      remarks,
    }),
  );
};

export default useParseFlightsData;
