import useAxios from "@src/common/axios/useAxios";
import { FLIGHT_ENDPOINT } from "@src/config/baseUrls";
import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

const useDeleteFlight = (flightId?: string) => {
  const axios = useAxios();
  return useMutation<AxiosResponse, AxiosError>(
    async () => await axios.delete(`${FLIGHT_ENDPOINT}/${flightId}`),
  );
};

export default useDeleteFlight;
