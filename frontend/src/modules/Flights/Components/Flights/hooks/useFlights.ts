import { AxiosError, AxiosResponse } from "axios";
import { useQuery } from "react-query";

import useAxios from "@src/common/axios/useAxios";
import {
  Landings,
  OperationalConditionTime,
  PathPoint,
  PilotFunctionTime,
  PilotTime,
  SyntheticTrainingDeviceSession,
} from "@src/modules/Flights/Components/Drafts/hooks/useDrafts";

import { FLIGHT_ENDPOINT } from "@src/config/baseUrls";

interface Aircraft {
  _id: string;
  name?: string;
  registration: string;
  type: string;
}

export interface FlightResponse {
  _id: string;
  departureDate: string;
  departurePoint: PathPoint;
  arrivalDate: string;
  arrivalPoint: PathPoint;

  aircraft: Aircraft;
  pic: string;
  landings: Landings;

  pilotTime: PilotTime<number>;
  pft: PilotFunctionTime<number>;
  oct: OperationalConditionTime<number>;
  ifrApproaches: number;
  stds?: SyntheticTrainingDeviceSession<number>;
  remarks?: string;
}

const useFlights = () => {
  const axios = useAxios();

  return useQuery<AxiosResponse<FlightResponse[]>, AxiosError>(
    "fetch-flights",
    async () => await axios.get(`${FLIGHT_ENDPOINT}/all`),
  );
};

export default useFlights;
