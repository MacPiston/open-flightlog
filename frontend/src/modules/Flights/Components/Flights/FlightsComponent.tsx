import React, { useState } from "react";
import { RowSelectedEvent } from "ag-grid-community";

import useNewDraftId from "@src/modules/Shared/hooks/useNewDraftId";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useFlights from "@src/modules/Flights/Components/Flights/hooks/useFlights";
import useParseFlightsData from "@src/modules/Flights/Components/Flights/hooks/useParseFlightsData";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useDeleteFlight from "@src/modules/Flights/Components/Flights/hooks/useDeleteFlight";

import flightsColumnDefs from "@src/modules/Flights/Components/Flights/dataGridConfig/columns";

import {
  FlightsTabPanel,
  FlightsTabFab,
  StyledButton,
} from "@src/modules/Flights/Components/Flights/FlightsComponent.components";
import Add from "@mui/icons-material/Add";
import DataGrid from "@src/modules/Shared/components/DataGrid/DataGrid";
import CustomLoadingOverlay from "@src/modules/Shared/components/DataGrid/CustomLoadingOverlay";
import DraftRow from "@src/modules/Flights/Components/Drafts/dataGridConfig/row";
import HeaderWithButtons from "@src/modules/Shared/components/HeaderWithButtons";

const FlightsComponent: React.FunctionComponent = () => {
  const [selectedFlightId, setSelectedFlightId] = useState<string | undefined>(
    undefined,
  );
  const { toDraft, toFlight } = useRedirects();
  const { error } = useNotifications();

  const { data, refetch } = useFlights();
  const flights = useParseFlightsData(data?.data);
  const newDraftMutation = useNewDraftId();
  const deleteFlightMutation = useDeleteFlight(selectedFlightId);

  const onGenerateNewDraftId = () => {
    newDraftMutation.mutate(undefined, {
      onSuccess: (response) => {
        toDraft(response.data._id);
      },
      onError: () => {
        error("Something went wrong");
      },
    });
  };

  const handleRowSelected = (e: RowSelectedEvent) => {
    const id = (e.data as DraftRow).id;
    if (id !== selectedFlightId) setSelectedFlightId(id);
  };

  const handleFinishEditing = () => {
    if (!selectedFlightId) return;
    toFlight(selectedFlightId);
  };

  const handleDelete = () => {
    if (!selectedFlightId) return;
    deleteFlightMutation.mutate(undefined, {
      onSuccess: async () => {
        await refetch();
      },
    });
  };

  return (
    <FlightsTabPanel>
      <HeaderWithButtons
        title="Flights"
        buttons={
          <>
            <StyledButton
              color="error"
              disabled={!selectedFlightId}
              onClick={handleDelete}
            >
              Delete
            </StyledButton>
            <StyledButton
              disabled={!selectedFlightId}
              onClick={handleFinishEditing}
            >
              View details
            </StyledButton>
          </>
        }
      />

      <DataGrid
        rows={flights}
        columns={flightsColumnDefs}
        frameworkComponents={{
          customLoadingOverlay: CustomLoadingOverlay,
        }}
        loadingOverlayComponent={"customLoadingOverlay"}
        loadingOverlayComponentParams={{
          loadingMessage: "Fetching your flights...",
        }}
        rowSelection="single"
        onRowSelected={handleRowSelected}
        onFirstDataRendered={(event) => event.columnApi.autoSizeAllColumns()}
      />
      <FlightsTabFab onClick={onGenerateNewDraftId}>
        <Add />
      </FlightsTabFab>
    </FlightsTabPanel>
  );
};

export default FlightsComponent;
