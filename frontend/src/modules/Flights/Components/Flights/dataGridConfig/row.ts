type FlightRow = {
  id: string;
  departureDate?: string;
  departurePlace?: string;

  arrivalDate?: string;
  arrivalPlace?: string;

  registration?: string;
  type?: string;
  pic?: string;

  landings_total?: string;
  landings_day?: string;
  landings_night?: string;

  pilotTime_SP_total?: string;
  pilotTime_SE?: string;
  pilotTime_ME?: string;
  pilotTime_MP?: string;

  // time_total?: string;

  pft_total?: string;
  pft_pic?: string;
  pft_copilot?: string;
  pft_dual?: string;
  pft_instructor?: string;

  oct_total?: string;
  oct_night?: string;
  oct_ifr?: string;

  ifrApproaches?: string;

  stds_time?: string;
  stds_date?: string;
  stds_type?: string;

  remarks?: string;
};

export default FlightRow;
