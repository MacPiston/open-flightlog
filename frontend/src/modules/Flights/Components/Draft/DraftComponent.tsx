import React from "react";
import { useParams } from "react-router";

import useRedirects from "@src/modules/Shared/hooks/useRedirects";

import { DraftComponentWrapper } from "@src/modules/Flights/Components/Draft/DraftComponent.components";
import DraftForm from "@src/modules/Flights/Components/Draft/DraftForm";

const DraftComponent: React.FunctionComponent = () => {
  const { draftId } = useParams();
  const { toDrafts } = useRedirects();

  if (!draftId) {
    toDrafts();
    return null;
  }

  return (
    <DraftComponentWrapper>
      <DraftForm draftId={draftId} />
    </DraftComponentWrapper>
  );
};

export default DraftComponent;
