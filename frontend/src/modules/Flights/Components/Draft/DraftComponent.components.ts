import styled from "styled-components";

import { Box } from "@mui/material";

export const DraftComponentWrapper = styled(Box)`
  position: relative;
  width: 100%;
  height: 100%;
  overflow-y: scroll;
`;
