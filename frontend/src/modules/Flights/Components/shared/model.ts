import {
  PathPoint,
  PilotTime,
  Landings,
  OperationalConditionTime,
  PilotFunctionTime,
  SyntheticTrainingDeviceSession,
} from "@src/modules/Flights/Components/Drafts/hooks/useDrafts";

export interface FlightFormModel {
  departureDate?: string;
  departurePoint?: PathPoint;
  arrivalDate?: string;
  arrivalPoint?: PathPoint;

  aircraftId?: string;
  pic?: string;
  landings?: Landings;

  pilotTime?: PilotTime<string>;
  pft?: PilotFunctionTime<string>;
  oct?: OperationalConditionTime<string>;
  ifrApproaches?: number;
  stds?: SyntheticTrainingDeviceSession<string>;
  remarks?: string;
}
