import React from "react";
import { FormikProps } from "formik";

import { validateBeforeFlight } from "@src/modules/Flights/Components/Draft/DraftForm/validation";

import { AircraftModel } from "@src/modules/Aircrafts/hooks/useAircraft";
import { FlightFormModel } from "@src/modules/Flights/Components/shared/model";

import { Divider } from "@mui/material";
import {
  CreateFlightButton,
  StyledForm,
  SubmitButton,
} from "@src/modules/Flights/Components/Draft/DraftForm/DraftForm.components";
import FirstRow from "@src/modules/Flights/Components/shared/FormContent/FirstRow";
import ForthRow from "@src/modules/Flights/Components/shared/FormContent/ForthRow";
import SecondRow from "@src/modules/Flights/Components/shared/FormContent/SecondRow";
import ThirdRow from "@src/modules/Flights/Components/shared/FormContent/ThirdRow";

import SaveIcon from "@mui/icons-material/Save";
import FifthRow from "@src/modules/Flights/Components/shared/FormContent/FifthRow";
import HeaderWithButtons from "@src/modules/Shared/components/HeaderWithButtons";

type Props = FormikProps<FlightFormModel> & {
  aircrafts: AircraftModel[];
  withCreateButton?: boolean;
  onSaveFlight?: () => void;
};

const FlightFormContent: React.FunctionComponent<Props> = ({
  isSubmitting,
  errors,
  aircrafts,
  values,
  onSaveFlight,
  withCreateButton,
}) => (
  <StyledForm>
    <HeaderWithButtons
      title="Draft"
      buttons={
        <>
          <SubmitButton
            disabled={Object.keys(errors).length !== 0}
            type="submit"
          >
            <SaveIcon />
            <span>{isSubmitting ? "Saving..." : "Save changes"}</span>
          </SubmitButton>
          {withCreateButton && (
            <CreateFlightButton
              disabled={validateBeforeFlight(values)}
              onClick={() => onSaveFlight?.()}
              type="button"
            >
              Create flight
            </CreateFlightButton>
          )}
        </>
      }
    />
    <FirstRow aircrafts={aircrafts} />
    <Divider variant="middle" />
    <SecondRow />
    <Divider variant="middle" />
    <ThirdRow />
    <Divider variant="middle" />
    <ForthRow />
    <Divider variant="middle" />
    <FifthRow />
    <Divider variant="middle" />
  </StyledForm>
);

export default FlightFormContent;
