import React from "react";
import { Field } from "formik";

import {
  FieldDescription,
  FieldRow,
  FormRow,
  SmallTextField,
} from "@src/modules/Flights/Components/Draft/DraftForm/DraftForm.components";
import { Divider } from "@mui/material";

const ForthRow = () => {
  return (
    <FormRow width="80%">
      <FieldRow vertical>
        <FieldDescription>Landings</FieldDescription>
        <FieldRow>
          <Field
            title="Day"
            name="landings.daytime"
            type="number"
            component={SmallTextField}
          />
          <Field
            title="Night"
            name="landings.nighttime"
            type="number"
            component={SmallTextField}
          />
        </FieldRow>
      </FieldRow>

      <Divider orientation="vertical" flexItem />

      <FieldRow vertical>
        <FieldDescription>IFR Approaches</FieldDescription>
        <Field
          title="IFR Appr."
          name="ifrApproaches"
          type="number"
          component={SmallTextField}
        />
      </FieldRow>

      <Divider orientation="vertical" flexItem />

      <FieldRow vertical>
        <FieldDescription>OCT</FieldDescription>
        <FieldRow>
          <Field
            title="Night"
            name="oct.night"
            type="time"
            component={SmallTextField}
          />
          <Field
            title="IFR"
            name="oct.ifr"
            type="time"
            component={SmallTextField}
          />
        </FieldRow>
      </FieldRow>
    </FormRow>
  );
};

export default ForthRow;
