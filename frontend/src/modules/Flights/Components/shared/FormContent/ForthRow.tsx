import React from "react";
import { Field } from "formik";

import {
  FieldDescription,
  FieldRow,
  FormRow,
  SmallTextField,
} from "@src/modules/Flights/Components/Draft/DraftForm/DraftForm.components";

const ThirdRow = () => (
  <FieldRow vertical flex={5}>
    <FieldDescription>Pilot function time</FieldDescription>
    <FormRow width="60%" elementSpacing={3}>
      <FieldRow vertical>
        <Field
          title="PIC"
          name="pft.pic"
          type="time"
          component={SmallTextField}
        />
      </FieldRow>
      <FieldRow vertical>
        <Field
          title="Co-Pilot"
          name="pft.coPilot"
          type="time"
          component={SmallTextField}
        />
      </FieldRow>
      <FieldRow vertical>
        <Field
          title="Dual"
          name="pft.dual"
          type="time"
          component={SmallTextField}
        />
      </FieldRow>
      <FieldRow vertical>
        <Field
          title="Instructor"
          name="pft.instructor"
          type="time"
          component={SmallTextField}
        />
      </FieldRow>
    </FormRow>
  </FieldRow>
);

export default ThirdRow;
