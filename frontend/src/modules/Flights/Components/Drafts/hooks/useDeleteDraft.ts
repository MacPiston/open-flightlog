import { useMutation } from "react-query";
import { AxiosError, AxiosResponse } from "axios";

import useAxios from "@src/common/axios/useAxios";
import { FLIGHT_DRAFT_ENDPOINT } from "@src/config/baseUrls";

const useDeleteDraft = (draftId?: string) => {
  const axios = useAxios();

  return useMutation<AxiosResponse, AxiosError>(
    async () => await axios.delete(`${FLIGHT_DRAFT_ENDPOINT}/${draftId}`),
  );
};

export default useDeleteDraft;
