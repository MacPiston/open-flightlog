import { dateFromISOString } from "@src/common/helpers/date";
import DraftRow from "@src/modules/Flights/Components/Drafts/dataGridConfig/row";
import { DraftResponse } from "@src/modules/Flights/Components/Drafts/hooks/useDrafts";

const useParseDraftsData = (drafts?: DraftResponse[]): DraftRow[] => {
  if (!drafts) return [];

  return drafts.map((draft) => ({
    id: draft._id,
    createdOn: dateFromISOString(draft.createdOn).toLocaleDateString(),
    departurePlace: draft.departurePoint?.icaoName,
    departureDate: dateFromISOString(draft.departureDate).toLocaleDateString(),
    arrivalPlace: draft.arrivalPoint?.icaoName,
    arrivalDate: dateFromISOString(draft.arrivalDate).toLocaleDateString(),
    pic: draft.pic,
  }));
};

export default useParseDraftsData;
