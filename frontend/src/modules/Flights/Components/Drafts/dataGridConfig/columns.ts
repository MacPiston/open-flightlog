import DraftRow from "@src/modules/Flights/Components/Drafts/dataGridConfig/row";
import { ExtendedColDef } from "@src/modules/Shared/components/DataGrid/defaults";

const fieldName = (name: keyof DraftRow) => name;

const draftsColumnDefs: ExtendedColDef[] = [
  {
    headerName: "Created",
    field: fieldName("createdOn"),
    editable: false,
  },
  {
    headerName: "Departure from",
    field: fieldName("departurePlace"),
    editable: false,
  },
  {
    headerName: "Departure date",
    field: fieldName("departureDate"),
    editable: false,
  },
  {
    headerName: "Arrival to",
    field: fieldName("arrivalPlace"),
    editable: false,
  },
  {
    headerName: "Arrival date",
    field: fieldName("arrivalDate"),
    editable: false,
  },
  {
    headerName: "PIC",
    field: fieldName("pic"),
    editable: false,
  },
];

export default draftsColumnDefs;
