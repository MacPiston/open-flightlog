type DraftRow = {
  id?: string;

  createdOn?: string;

  departurePlace?: string;
  departureDate?: string;
  arrivalPlace?: string;
  arrivalDate?: string;

  pic?: string;
};

export default DraftRow;
