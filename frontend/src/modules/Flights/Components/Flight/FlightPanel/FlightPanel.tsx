import React from "react";
import { Box, Divider } from "@mui/material";

import {
  dateFromISOString,
  minutesToTimeString,
} from "@src/common/helpers/date";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import { FlightResponse } from "@src/modules/Flights/Components/Flights/hooks/useFlights";

import {
  InformationHeader,
  PanelRow,
  PanelWrapper,
  VerticalWrapper,
} from "@src/modules/Flights/Components/Flight/FlightPanel/FlightPanel.components";
import { Sub1Bold } from "@src/modules/Shared/components/Typography";

interface Props {
  data: FlightResponse;
}

const FlightPanel: React.FunctionComponent<Props> = ({ data }) => {
  const { toAircraft } = useRedirects();

  return (
    <PanelWrapper>
      <PanelRow>
        <VerticalWrapper>
          <InformationHeader>Basic informations</InformationHeader>
          <Box>
            <span>Departure from </span>
            <a
              href={`https://www.google.com/search?q=${data.departurePoint.icaoName}`}
              className="detail"
              target="_blank"
              rel="noreferrer"
            >
              {data.departurePoint.icaoName}
            </a>
            <span> at </span>
            <span className="detail">
              {dateFromISOString(data.departureDate).toDateString()}
            </span>
          </Box>
          <Box>
            <span>Arrival to </span>
            <a
              href={`https://www.google.com/search?q=${data.arrivalPoint.icaoName}`}
              className="detail"
              target="_blank"
              rel="noreferrer"
            >
              {data.arrivalPoint.icaoName}
            </a>
            <span> at </span>
            <span className="detail">
              {dateFromISOString(data.arrivalDate).toDateString()}
            </span>
          </Box>
        </VerticalWrapper>

        <Divider orientation="vertical" variant="middle" flexItem />

        <VerticalWrapper>
          <InformationHeader
            $hasLink
            onClick={() => toAircraft(data.aircraft._id)}
          >
            Aircraft
          </InformationHeader>
          <Box>
            <span>Type - </span>
            <span>{data.aircraft.type}</span>
          </Box>
          <Box>
            <span>Registration - </span>
            <span>{data.aircraft.type}</span>
          </Box>
        </VerticalWrapper>
      </PanelRow>

      <Divider variant="middle" />

      <PanelRow>
        <VerticalWrapper>
          <InformationHeader>Flight times</InformationHeader>
          <PanelRow spaceContent>
            <VerticalWrapper centerText>
              <span>Single engine</span>
              <Sub1Bold>
                {minutesToTimeString(data.pilotTime.spse) || "-"}
              </Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Multi engine</span>
              <Sub1Bold>
                {minutesToTimeString(data.pilotTime.spme) || "-"}
              </Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Multi pilot</span>
              <Sub1Bold>
                {minutesToTimeString(data.pilotTime.mp) || "-"}
              </Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Total time</span>
              <Sub1Bold className="detail">
                {minutesToTimeString(
                  (data.pilotTime.spse || 0) +
                    (data.pilotTime.spme || 0) +
                    (data.pilotTime.mp || 0),
                )}
              </Sub1Bold>
            </VerticalWrapper>
          </PanelRow>
        </VerticalWrapper>

        <Divider orientation="vertical" variant="middle" flexItem />

        <VerticalWrapper centerText>
          <InformationHeader>Pilot-in-command</InformationHeader>
          <Box>
            <span>{data.pic}</span>
          </Box>
        </VerticalWrapper>
      </PanelRow>

      <Divider variant="middle" />

      <PanelRow>
        <VerticalWrapper>
          <InformationHeader>Landings</InformationHeader>
          <PanelRow spaceContent>
            <VerticalWrapper centerText>
              <span>Nighttime</span>
              <Sub1Bold>{data.landings.nighttime || "-"}</Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Daytime</span>
              <Sub1Bold>{data.landings.daytime || "-"}</Sub1Bold>
            </VerticalWrapper>
          </PanelRow>
        </VerticalWrapper>

        <Divider orientation="vertical" variant="middle" flexItem />

        <VerticalWrapper centerText>
          <InformationHeader>IFR Approaches</InformationHeader>
          <Sub1Bold>{data.ifrApproaches || "-"}</Sub1Bold>
        </VerticalWrapper>

        <Divider orientation="vertical" variant="middle" flexItem />

        <VerticalWrapper>
          <InformationHeader>Operational conditional time</InformationHeader>
          <PanelRow spaceContent>
            <VerticalWrapper centerText>
              <span>Night</span>
              <Sub1Bold>{minutesToTimeString(data.oct.night) || "-"}</Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>IFR</span>
              <Sub1Bold>{minutesToTimeString(data.oct.ifr) || "-"}</Sub1Bold>
            </VerticalWrapper>
          </PanelRow>
        </VerticalWrapper>
      </PanelRow>

      <Divider variant="middle" />

      <PanelRow>
        <VerticalWrapper>
          <InformationHeader>Pilot function time</InformationHeader>
          <PanelRow spaceContent>
            <VerticalWrapper centerText>
              <span>PIC</span>
              <Sub1Bold>{minutesToTimeString(data.pft.pic) || "-"}</Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Co-Pilot</span>
              <Sub1Bold>
                {minutesToTimeString(data.pft.coPilot) || "-"}
              </Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Dual</span>
              <Sub1Bold>{minutesToTimeString(data.pft.dual) || "-"}</Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Instructor</span>
              <Sub1Bold>
                {minutesToTimeString(data.pft.instructor) || "-"}
              </Sub1Bold>
            </VerticalWrapper>
          </PanelRow>
        </VerticalWrapper>
      </PanelRow>

      <Divider variant="middle" />

      <PanelRow>
        <VerticalWrapper>
          <InformationHeader>
            Synthetic training devices session
          </InformationHeader>
          <PanelRow spaceContent>
            <VerticalWrapper centerText>
              <span>Date</span>
              <Sub1Bold>
                {data.stds?.date
                  ? dateFromISOString(data.stds?.date).toDateString()
                  : "-"}
              </Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Type</span>
              <Sub1Bold>{data.stds?.type || "-"}</Sub1Bold>
            </VerticalWrapper>
            <VerticalWrapper centerText>
              <span>Time</span>
              <Sub1Bold>
                {minutesToTimeString(data.stds?.totalTime) || "-"}
              </Sub1Bold>
            </VerticalWrapper>
          </PanelRow>
        </VerticalWrapper>

        <Divider orientation="vertical" variant="middle" flexItem />

        <VerticalWrapper centerText>
          <InformationHeader>Remarks</InformationHeader>
          <Box>
            <span>{data.remarks || "-"}</span>
          </Box>
        </VerticalWrapper>
      </PanelRow>
    </PanelWrapper>
  );
};

export default FlightPanel;
