import styled from "styled-components";

import { Box, Button } from "@mui/material";

export const FlightComponentWrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  position: relative;
  width: 100%;
  height: 100%;
  padding-top: 0;

  overflow-y: auto;

  & > hr {
    margin-bottom: ${({ theme }) => theme.spacing(2)};
  }
`;

export const StyledButton = styled(Button).attrs({ variant: "contained" })``;
