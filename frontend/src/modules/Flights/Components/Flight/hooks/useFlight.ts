import { useQuery } from "react-query";

import useAxios from "@src/common/axios/useAxios";
import { AxiosError, AxiosResponse } from "axios";
import { FlightResponse } from "@src/modules/Flights/Components/Flights/hooks/useFlights";
import { FLIGHT_ENDPOINT } from "@src/config/baseUrls";

const useFlight = (flightId: string) => {
  const axios = useAxios();

  return useQuery<AxiosResponse<FlightResponse>, AxiosError>(
    "fetch-flight",
    async () => await axios.get(`${FLIGHT_ENDPOINT}/${flightId}`),
  );
};

export default useFlight;
