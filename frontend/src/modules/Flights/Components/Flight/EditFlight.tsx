import React from "react";
import { Formik } from "formik";

import useAircrafts from "@src/modules/Aircrafts/hooks/useAircrafts";

import FlightFormContent from "@src/modules/Flights/Components/shared/FormContent/FlightFormContent";
import useParseFlightData from "@src/modules/Flights/Components/Flight/hooks/useParseFlightData";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import Spinner from "@src/modules/Shared/components/Spinner";
import { FlightResponse } from "@src/modules/Flights/Components/Flights/hooks/useFlights";
import useUpdateFlight from "@src/modules/Flights/Components/Flight/hooks/useUpdateFlight";
import { flightValidationSchema } from "@src/modules/Flights/Components/Draft/DraftForm/validation";

interface Props {
  flightData: FlightResponse;
  refetch: () => void;
}

const EditFlight: React.FunctionComponent<Props> = ({
  flightData,
  refetch,
}) => {
  const { error, success } = useNotifications();
  const { toFlights } = useRedirects();
  const {
    data: aircrafts,
    isLoading: aircraftsLoading,
    error: aircraftsError,
  } = useAircrafts();
  const flight = useParseFlightData(flightData);
  const updateFlight = useUpdateFlight(
    flightData._id,
    () => {
      success("Flight updated");
      refetch();
    },
    () => error("Failed to update"),
  );

  if (aircraftsLoading) return <Spinner />;

  if (aircraftsError) {
    error("Error fetching data");
    toFlights();
    return null;
  }

  return (
    <Formik
      initialValues={{ ...flight! }}
      validationSchema={flightValidationSchema}
      onSubmit={updateFlight}
    >
      {(formProps) => (
        <FlightFormContent aircrafts={aircrafts!.data} {...formProps} />
      )}
    </Formik>
  );
};

export default EditFlight;
