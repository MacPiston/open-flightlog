import React from "react";
import { Outlet } from "react-router-dom";

import { FlightsOutletWrapper } from "@src/modules/Flights/Flights.components";

const FlightComponents = () => (
  <FlightsOutletWrapper>
    <Outlet />
  </FlightsOutletWrapper>
);

export default FlightComponents;
