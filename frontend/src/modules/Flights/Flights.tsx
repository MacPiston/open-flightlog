import React, { lazy } from "react";
import { Route, Routes, Navigate } from "react-router-dom";

import { flightsRouting } from "@src/modules/Flights/subrouting";

import FlightComponents from "@src/modules/Flights/Components/FlightsComponents";

const Flights: React.FunctionComponent = () => (
  <Routes>
    <Route element={<FlightComponents />}>
      {flightsRouting.map((route, index) => {
        const Component = lazy(route.importComponent);

        return <Route key={index} path={route.path} element={<Component />} />;
      })}
      <Route path="*" element={<Navigate to="/" />} />
    </Route>
  </Routes>
);

export default Flights;
