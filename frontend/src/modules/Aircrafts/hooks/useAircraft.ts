import { AxiosResponse, AxiosError } from "axios";
import { useQuery } from "react-query";

import useAxios from "@src/common/axios/useAxios";
import { AIRCRAFT_ENDPOINT } from "@src/config/baseUrls";

export interface AircraftModel {
  _id: string;
  name: string;
  registration: string;
  type: string;
  flights: unknown[];
}

const useAircraft = (aircraftId: string) => {
  const axios = useAxios();

  return useQuery<AxiosResponse<AircraftModel>, AxiosError>(
    "fetch-aircraft",
    async () => await axios.get(`${AIRCRAFT_ENDPOINT}/${aircraftId}`),
  );
};

export default useAircraft;
