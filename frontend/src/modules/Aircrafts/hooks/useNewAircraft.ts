import useAxios from "@src/common/axios/useAxios";
import { AIRCRAFT_ENDPOINT } from "@src/config/baseUrls";
import { AircraftModel } from "@src/modules/Aircrafts/hooks/useAircraft";
import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

export interface NewAircraftModel {
  name?: string;
  registration: string;
  type: string;
}

const useNewAircraft = () => {
  const axios = useAxios();

  return useMutation<
    AxiosResponse<AircraftModel>,
    AxiosError,
    NewAircraftModel
  >(async (values) => await axios.post(AIRCRAFT_ENDPOINT, values));
};

export default useNewAircraft;
