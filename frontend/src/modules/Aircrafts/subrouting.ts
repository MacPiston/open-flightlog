import routesPaths from "@src/modules/Routing/routesPaths";
import { SubRouteProps } from "@src/modules/Shared/types";

export const aircraftsRouting: SubRouteProps[] = [
  {
    label: "Aircrafts",
    path: routesPaths.aircraftsPaths.AIRCRAFTS,
    importComponent: () =>
      import("@src/modules/Aircrafts/Components/Aircrafts"),
  },
  {
    label: "New Aircraft",
    path: routesPaths.aircraftsPaths.NEW,
    importComponent: () =>
      import("@src/modules/Aircrafts/Components/NewAircraft"),
  },
];
