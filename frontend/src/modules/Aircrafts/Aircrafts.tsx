import React, { lazy } from "react";
import { Routes, Route, Navigate } from "react-router-dom";

import AircraftsComponents from "@src/modules/Aircrafts/Components/AircraftsComponents";
import { aircraftsRouting } from "@src/modules/Aircrafts/subrouting";

const Aircrafts: React.FunctionComponent = () => (
  <Routes>
    <Route element={<AircraftsComponents />} />
    {aircraftsRouting.map((route, index) => {
      const Component = lazy(route.importComponent);

      return <Route key={index} path={route.path} element={<Component />} />;
    })}
    <Route path="*" element={<Navigate to="/" />} />
  </Routes>
);

export default Aircrafts;
