import styled from "styled-components";
import { Paper, Divider, Stack, List } from "@mui/material";
import { Box } from "@mui/system";

import { TOPBAR_HEIGHT } from "@src/modules/App/AppTopbar/AppTopbar";

export const MainWrapper = styled(Paper)`
  padding: ${({ theme }) => theme.spacing(2)};
  width: 768px;
  min-height: 128px;
  height: 50%;
  max-height: calc(100vh - ${TOPBAR_HEIGHT}px - 128px);

  display: flex;
  flex-direction: column;
`;

export const StyledVerticalDivider = styled(Divider)`
  margin: 0 ${({ theme }) => theme.spacing(2)};
`;

export const StyledRow = styled(Box)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0 ${({ theme }) => theme.spacing(1)};
`;

export const DetailsList = styled(List)`
  flex: 5;
  padding-left: ${({ theme }) => theme.spacing(2)};
`;

export const ImageContainer = styled(Box)`
  flex: 4;
`;

export const ImagePlaceholder = styled(Box)`
  margin: 0 auto;
  height: 192px;
  width: 192px;
  background-color: gray;
`;

export const DetailsStack = styled(Stack)`
  flex-grow: 1;
  width: 100%;
`;

export const GridWrapper = styled(Box)`
  flex-grow: 1;
`;
