import React from "react";
import { useParams } from "react-router";
import { Navigate } from "react-router-dom";

import routesPaths from "@src/modules/Routing/routesPaths";

import {
  ImageContainer,
  MainWrapper,
  StyledVerticalDivider,
  StyledRow,
  ImagePlaceholder,
  DetailsStack,
  GridWrapper,
} from "@src/modules/Aircrafts/Components/Aircraft/AircraftComponent.components";
import Header from "@src/modules/Shared/components/Header";
import Spinner from "@src/modules/Shared/components/Spinner";
import AircraftDetails from "@src/modules/Aircrafts/Components/Aircraft/AircraftDetails";
import AircraftDataGrid from "@src/modules/Aircrafts/Components/Aircraft/AircraftDataGrid";

import CloseIcon from "@mui/icons-material/Close";

import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useAircraft from "@src/modules/Aircrafts/hooks/useAircraft";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";

const AircraftComponent = () => {
  const { aircraftId } = useParams();
  const { toAircrafts } = useRedirects();
  const { error } = useNotifications();
  const { isLoading, data, isError } = useAircraft(aircraftId ?? "");

  if (!aircraftId) return <Navigate to={routesPaths.aircraftsPaths.MAIN} />;

  if (isError) {
    error("Aircraft not found");
    toAircrafts();
  }

  return (
    <MainWrapper>
      <Header
        title={data?.data.name || data?.data.registration || null}
        icon={<CloseIcon />}
        iconOnClick={toAircrafts}
      />
      {isLoading ? (
        <Spinner />
      ) : (
        <DetailsStack spacing={2}>
          <StyledRow>
            <AircraftDetails
              type={data?.data.type || ""}
              registration={data?.data.registration || ""}
              flights={data?.data.flights.length || -1}
              lastFlight={new Date()}
            />
            <StyledVerticalDivider orientation="vertical" flexItem />
            <ImageContainer>
              <ImagePlaceholder />
            </ImageContainer>
          </StyledRow>
          <GridWrapper>
            <AircraftDataGrid />
          </GridWrapper>
        </DetailsStack>
      )}
    </MainWrapper>
  );
};

export default AircraftComponent;
