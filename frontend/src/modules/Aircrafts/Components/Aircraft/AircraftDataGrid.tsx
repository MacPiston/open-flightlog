import React from "react";

import { DataGrid, GridColDef, GridRowsProp } from "@mui/x-data-grid";

const columns: GridColDef[] = [
  {
    field: "nr",
    headerName: "#",
    headerAlign: "center",
  },
  {
    field: "date",
    headerName: "Date",
    headerAlign: "center",
  },
  {
    field: "duration",
    headerName: "Duration",
    headerAlign: "center",
  },
  {
    field: "from",
    headerName: "From",
    headerAlign: "center",
  },
  {
    field: "to",
    headerName: "To",
    headerAlign: "center",
  },
];

const rows: GridRowsProp = [
  //   {
  //     id: 1,
  //     nr: 1,
  //   },
  //   {
  //     id: 2,
  //     nr: 2,
  //   },
  //   {
  //     id: 3,
  //     nr: 3,
  //   },
  //   {
  //     id: 4,
  //     nr: 4,
  //   },
  //   {
  //     id: 5,
  //     nr: 5,
  //   },
  //   {
  //     id: 6,
  //     nr: 6,
  //   },
  //   {
  //     id: 7,
  //     nr: 7,
  //   },
  //   {
  //     id: 8,
  //     nr: 8,
  //   },
];

const AircraftDataGrid = () => (
  <DataGrid isRowSelectable={() => false} columns={columns} rows={rows} />
);

export default AircraftDataGrid;
