import styled from "styled-components";

import { Box } from "@mui/material";

export const NewAircraftComponentWrapper = styled(Box)``;

export const FormFieldsWrapper = styled(Box)`
  display: flex;

  justify-content: center;
`;
