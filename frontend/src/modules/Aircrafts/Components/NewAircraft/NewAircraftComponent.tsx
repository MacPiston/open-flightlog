import React, { useCallback } from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { Button } from "@mui/material";

import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useNewAircraft, {
  NewAircraftModel,
} from "@src/modules/Aircrafts/hooks/useNewAircraft";

import {
  FormFieldsWrapper,
  NewAircraftComponentWrapper,
} from "@src/modules/Aircrafts/Components/NewAircraft/NewAircraftComponent.components";
import NewAircraftFormContent from "@src/modules/Aircrafts/Components/NewAircraft/NewAircraftFormContent";
import HeaderWithButtons from "@src/modules/Shared/components/HeaderWithButtons";

const initialValues: NewAircraftModel = {
  name: "",
  registration: "",
  type: "",
};

const NewAircraftComponent: React.FunctionComponent = () => {
  const mutation = useNewAircraft();
  const { success, error } = useNotifications();
  const { toAircrafts } = useRedirects();

  const validationSchema: Yup.SchemaOf<NewAircraftModel> = Yup.object().shape({
    name: Yup.string().optional(),
    registration: Yup.string().required("Registration required"),
    type: Yup.string().required("Type required"),
  });

  const handleSubmit = useCallback(
    async (values: NewAircraftModel) =>
      mutation.mutate(values, {
        onSuccess: () => {
          success("Aircraft created!");
          toAircrafts();
        },
        onError: (err) => {
          error(err.response?.data.message);
        },
      }),
    [mutation, success, error, toAircrafts],
  );

  return (
    <NewAircraftComponentWrapper>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(formProps) => (
          <Form>
            <HeaderWithButtons
              title="Add new aircraft"
              buttons={
                <Button type="submit" variant="contained">
                  Save
                </Button>
              }
            />
            <FormFieldsWrapper>
              <NewAircraftFormContent {...formProps} />
            </FormFieldsWrapper>
          </Form>
        )}
      </Formik>
    </NewAircraftComponentWrapper>
  );
};

export default NewAircraftComponent;
