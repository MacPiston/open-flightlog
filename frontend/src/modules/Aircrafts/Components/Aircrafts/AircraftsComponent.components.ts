import { Grid, styled } from "@mui/material";
import { Box } from "@mui/system";
import ColoredFab from "@src/modules/Shared/components/ColoredFab";

export const AircraftsWrapper = styled(Box)`
  position: relative;
  height: 100%;
  width: 100%;
`;

export const StyledGrid = styled(Grid)`
  justify-content: space-evenly;
`;

export const StyledFab = styled(ColoredFab)`
  position: absolute;
  bottom: 0;
  right: 0;

  transform: translateX(calc(var(--var-padding) * -1))
    translateY(calc(var(--var-padding) * -1));
`;
