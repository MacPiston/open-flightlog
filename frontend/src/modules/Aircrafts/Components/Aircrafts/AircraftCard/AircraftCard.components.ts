import styled from "styled-components";

import { Card, CardContent } from "@mui/material";
import { Box } from "@mui/system";

export const StyledCard = styled(Card)``;

export const StyledCardContent = styled(CardContent)`
  padding: ${({ theme }) => theme.spacing(2)} !important;
`;

export const InfoBox = styled(Box)`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  /* cursor: pointer; */

  margin-top: ${({ theme }) => theme.spacing(2)};

  ${({ theme }) => theme.breakpoints.down("sm")} {
    & > *:not(:first-child) {
      margin-left: 0.5rem;
    }
  }

  ${({ theme }) => theme.breakpoints.up("sm")} {
    flex-direction: column;
  }
`;

export const InfoRow = styled(Box)`
  h6:nth-child(2) {
    margin-left: ${({ theme }) => theme.spacing(2)};
  }
`;
