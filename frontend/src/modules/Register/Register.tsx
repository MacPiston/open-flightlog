import React from "react";

import { RegisterWrapper } from "@src/modules/Register/Register.components";
import RegisterForm from "@src/modules/Register/RegisterForm";

const Register = () => (
  <RegisterWrapper>
    <RegisterForm />
  </RegisterWrapper>
);

export default Register;
