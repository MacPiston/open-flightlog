import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

import useAxios from "@src/common/axios/useAxios";
import { RegisterResponse } from "@src/common/helpers/userToken";
import { USER_ENDPOINT } from "@src/config/baseUrls";

export interface RegisterModel {
  username: string;
  email: string;
  password: string;
}

const useRegister = () => {
  const axios = useAxios();

  return useMutation<
    AxiosResponse<RegisterResponse>,
    AxiosError,
    RegisterModel
  >(async (values) => await axios.post(USER_ENDPOINT, values));
};

export default useRegister;
