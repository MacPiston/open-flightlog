import axios, { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

import { ACTIVATE_ENDPOINT } from "@src/config/baseUrls";

export interface ActivateModel {
  token: string;
}

const useActivate = () => {
  return useMutation<AxiosResponse, AxiosError, ActivateModel>(
    async (values) => await axios.post(ACTIVATE_ENDPOINT, values),
  );
};

export default useActivate;
