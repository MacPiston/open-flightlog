import React, { useEffect } from "react";

import { Typography } from "@mui/material";
import { Navigate, useParams } from "react-router";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useActivate from "@src/modules/Register/hooks/useActivate";
import routesPaths from "@src/modules/Routing/routesPaths";

const Activate = () => {
  const { activationToken } = useParams();
  const { toLanding } = useRedirects();
  const { error, success } = useNotifications();
  const mutation = useActivate();

  useEffect(() => {
    const activate = async () => {
      if (!activationToken) return;
      mutation.mutate(
        { token: activationToken },
        {
          onSuccess: () => {
            success("Account activated - you can now log in!");
            toLanding();
          },
          onError: (err) => {
            error(`Couldn't activate account: ${err.response?.data.message}`);
            setTimeout(() => toLanding(), 3000);
          },
        },
      );
    };

    activate();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activationToken, error, success, toLanding]);

  if (!activationToken) {
    error("Bad activation token");
    return <Navigate to={routesPaths.LANDING} />;
  }

  return (
    <Typography style={{ textAlign: "center" }}>
      It you see this message for more than 10s and there was no error, open the
      activation link again.
    </Typography>
  );
};

export default Activate;
