import React, { useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import RegisterFormContent, {
  RegisterFormModel,
} from "@src/modules/Register/RegisterForm/RegisterFormContent";
import useRegister from "@src/modules/Register/hooks/useRegister";
import {
  RegisterFormBackground,
  SuccessWrapper,
} from "@src/modules/Register/RegisterForm/RegisterForm.components";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import { Typography } from "@mui/material";

const initialValues: RegisterFormModel = {
  username: "",
  email: "",
  password: "",
  password2: "",
};

const RegisterForm = () => {
  const mutation = useRegister();
  const { error } = useNotifications();
  const [success, setSuccess] = useState<boolean>(false);

  const handleSubmit = ({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    password2,
    ...values
  }: RegisterFormModel & { password2: string }) =>
    mutation.mutate(values, {
      onSuccess: () => setSuccess(true),
      onError: (err) => {
        error(`Error during registration: ${err.message}`);
      },
    });

  const validationSchema: Yup.SchemaOf<RegisterFormModel> = Yup.object().shape({
    username: Yup.string().required("Username is required").min(5, "Too short"),
    email: Yup.string().email("Invalid e-mail").required("E-mail required"),
    password: Yup.string()
      .min(6, "Too short!")
      .max(100, "Too long!")
      .required("Password required"),
    password2: Yup.string()
      .required("Password confirmation required")
      .oneOf([Yup.ref("password"), null], "Passwords must match"),
  });

  return (
    <RegisterFormBackground>
      {success ? (
        <SuccessWrapper>
          <Typography variant="h4">Registration complete!</Typography>
          <Typography variant="h6">
            We&apos;ve sent you an email containing confirmation link. Check
            your inbox and open the link to finalize the registration.
          </Typography>
        </SuccessWrapper>
      ) : (
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {(formProps) => <RegisterFormContent {...formProps} />}
        </Formik>
      )}
    </RegisterFormBackground>
  );
};

export default RegisterForm;
