import React from "react";
import { ListItemIcon, Menu, MenuItem, MenuProps } from "@mui/material";

import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useIsAuthed from "@src/modules/App/hooks/useIsAuthed";

import AppEventTarget from "@src/common/events/AppEventTarget";
import LogoutRequestEvent from "@src/common/events/LogoutRequestEvent";

import {
  LogoutOutlined,
  SvgIconComponent,
  Add,
  AccountBoxOutlined,
} from "@mui/icons-material";

interface MenuItemInterface {
  text?: string;
  component?: Element;
  icon?: SvgIconComponent;
  onClick: () => void;
}

const ProfileMenu: React.FunctionComponent<MenuProps> = (props) => {
  const isAuthed = useIsAuthed();
  const { toRegister, toLanding } = useRedirects();

  const authedMenuItems: MenuItemInterface[] = [
    {
      text: "Log out",
      icon: LogoutOutlined,
      onClick: () => AppEventTarget.dispatchEvent(new LogoutRequestEvent()),
    },
  ];

  const unauthedMenuItems: MenuItemInterface[] = [
    {
      text: "Create account",
      icon: AccountBoxOutlined,
      onClick: toRegister,
    },
    {
      text: "Log in",
      icon: Add,
      onClick: toLanding,
    },
  ];

  const menuItems = isAuthed ? authedMenuItems : unauthedMenuItems;

  return (
    <Menu {...props}>
      {menuItems.map((e, i) => (
        <MenuItem key={i} onClick={e.onClick}>
          {e.icon && (
            <ListItemIcon>
              <e.icon />
            </ListItemIcon>
          )}
          {e.text}
          {e.component}
        </MenuItem>
      ))}
    </Menu>
  );
};

export default ProfileMenu;
