import { useContext } from "react";

import AppContext from "@src/modules/App/AppContext";

const useAppContext = () => useContext(AppContext);

export default useAppContext;
