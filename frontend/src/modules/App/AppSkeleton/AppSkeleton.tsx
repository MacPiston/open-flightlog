import React, { useState } from "react";
import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";

import {
  PageContentWrapper,
  Wrapper,
} from "@src/modules/App/AppSkeleton/AppSkeleton.components";
import AppTopbar from "@src/modules/App/AppTopbar";
import AppDrawer from "@src/modules/App/AppDrawer/AppDrawer";
import { DRAWER_WIDTH } from "@src/modules/App/AppDrawer/AppDrawer.components";

const AppSkeleton = () => {
  const [mobileDrawerOpen, setMobileDrawerOpen] = useState(false);

  const toggleMobileDrawer = () => setMobileDrawerOpen((prev) => !prev);

  const closeMobileDrawer = () => setMobileDrawerOpen(false);

  return (
    <Wrapper>
      <Box sx={{ display: "flex" }}>
        <AppTopbar toggleMobileDrawer={toggleMobileDrawer} />
        <AppDrawer
          mobileDrawerOpen={mobileDrawerOpen}
          toggleMobileDrawer={toggleMobileDrawer}
          closeMobileDrawer={closeMobileDrawer}
        />
        <PageContentWrapper
          sx={{ width: { md: `calc(100vw - ${DRAWER_WIDTH}px)` }, flexGrow: 1 }}
        >
          <Outlet />
        </PageContentWrapper>
      </Box>
    </Wrapper>
  );
};

export default AppSkeleton;
