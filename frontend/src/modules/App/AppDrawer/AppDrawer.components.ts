import { Drawer, styled } from "@mui/material";

export const DRAWER_WIDTH = 210;

export const StyledDrawer = styled(Drawer)`
  & > .MuiDrawer-paper {
    background-color: ${({ theme }) => theme.palette.primary.main} !important;
  }

  & .MuiDrawer-paper {
    box-sizing: border-box;
    width: ${DRAWER_WIDTH}px;
  }

  .Mui-selected {
    background-color: ${({ theme }) => theme.palette.secondary.main} !important;
  }
`;
