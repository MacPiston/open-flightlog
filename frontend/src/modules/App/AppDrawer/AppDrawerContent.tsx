import React from "react";
import { Box, Divider, List, Toolbar } from "@mui/material";

import DrawerLink, {
  DrawerLinkProps,
} from "@src/modules/App/AppDrawer/DrawerLink";
import useIsAuthed from "@src/modules/App/hooks/useIsAuthed";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import routesPaths from "@src/modules/Routing/routesPaths";
import { StyledLogo } from "@src/modules/Shared/components/Logo";

import {
  Add,
  AssignmentTurnedInOutlined,
  FeedOutlined,
  Flight,
  FlightTakeoffOutlined,
  HomeOutlined,
  HowToRegOutlined,
  ListAltOutlined,
  Login,
} from "@mui/icons-material";

interface DrawerLinkPropsExtended extends Omit<DrawerLinkProps, "text"> {
  isDivider?: boolean;
  text?: string;
}

interface Props {
  closeMobileDrawer?: () => void;
}

const AppDrawerContent: React.FunctionComponent<Props> = ({
  closeMobileDrawer,
}) => {
  const isAuthed = useIsAuthed();
  const {
    toRegister,
    toLanding,
    toNewAircraft,
    toAircrafts,
    toDrafts,
    toFlights,
    toHome,
  } = useRedirects();

  const publicItems: DrawerLinkPropsExtended[] = [
    {
      text: "Create account",
      icon: <HowToRegOutlined />,
      action: { onClick: toRegister },
      matchedRoute: routesPaths.REGISTER,
    },
    {
      text: "Login",
      icon: <Login />,
      action: { onClick: toLanding },
      matchedRoute: routesPaths.LANDING,
    },
  ];

  const authedItems: DrawerLinkPropsExtended[] = [
    {
      text: "Home",
      icon: <HomeOutlined />,
      action: { onClick: toHome },
      matchedRoute: routesPaths.HOME,
    },
    {
      isDivider: true,
    },
    {
      text: "Add aircraft",
      icon: <Add />,
      action: { onClick: toNewAircraft },
      matchedRoute: `${routesPaths.AIRCRAFTS}${routesPaths.aircraftsPaths.NEW}`,
      addAction: true,
    },
    {
      text: "Aircrafts",
      icon: <Flight />,
      action: { onClick: toAircrafts },
      matchedRoute: routesPaths.aircraftsPaths.MAIN,
    },
    {
      isDivider: true,
    },
    {
      text: "Create draft",
      icon: <Add />,
      addAction: true,
    },
    {
      text: "Latest draft",
      icon: <AssignmentTurnedInOutlined />,
    },
    {
      text: "Drafts",
      icon: <FeedOutlined />,
      action: { onClick: toDrafts },
      matchedRoute: `${routesPaths.FLIGHTS}${routesPaths.flightsPaths.DRAFTS_LIST}`,
    },
    {
      isDivider: true,
    },
    {
      text: "Latest flight",
      icon: <ListAltOutlined />,
    },
    {
      text: "Flights",
      icon: <FlightTakeoffOutlined />,
      action: { onClick: toFlights },
      matchedRoute: routesPaths.FLIGHTS,
    },
  ];

  const drawerItems = isAuthed ? authedItems : publicItems;

  return (
    <Box>
      <Toolbar>
        <Box sx={{ display: "flex" }}>
          <StyledLogo />
        </Box>
      </Toolbar>

      <List sx={{ paddingTop: 0 }}>
        <Divider />

        {drawerItems.map(({ action, isDivider, ...rest }, i) =>
          isDivider ? (
            <Divider key={i} />
          ) : (
            <DrawerLink
              key={i}
              action={
                action?.onClick
                  ? {
                      onClick: () => {
                        closeMobileDrawer?.();
                        action.onClick();
                      },
                    }
                  : action
              }
              {...rest}
            />
          ),
        )}
        <Divider />
      </List>
    </Box>
  );
};

export default AppDrawerContent;
