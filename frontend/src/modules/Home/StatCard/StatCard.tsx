import React from "react";

import { StatCardWrapper } from "@src/modules/Home/StatCard/StatCard.components";
import { H6, Sub1Standard } from "@src/modules/Shared/components/Typography";

interface Props {
  text: string;
  value: string;
}

const StatCard: React.FunctionComponent<Props> = ({ text, value }) => (
  <StatCardWrapper>
    <Sub1Standard>{text}</Sub1Standard>
    <H6>{value}</H6>
  </StatCardWrapper>
);

export default StatCard;
