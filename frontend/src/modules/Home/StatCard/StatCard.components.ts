import { Box, styled } from "@mui/material";

export const StatCardWrapper = styled(Box)`
  display: flex;
  align-items: center;
  flex-direction: column;

  padding: ${({ theme }) => theme.spacing(2)} ${({ theme }) => theme.spacing(3)};

  border: 1px solid ${({ theme }) => theme.palette.grey[500]};
  border-radius: 12px;
  box-shadow: ${({ theme }) => theme.shadows[3]};
`;
