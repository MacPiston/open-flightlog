import { Box, styled } from "@mui/material";

export const HomeWrapper = styled(Box)`
  & > hr {
    margin: ${({ theme }) => theme.spacing(3)} 0;
  }
`;

export const HeaderWrapper = styled(Box)`
  & > h5 {
    margin-top: ${({ theme }) => theme.spacing(1)};
    margin-left: ${({ theme }) => theme.spacing(1)};
  }
`;

export const VerticalWrapper = styled(Box)`
  display: flex;
  flex-direction: column;

  & > h6 {
    margin-bottom: ${({ theme }) => theme.spacing(1)};
  }
`;

export const StatsWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(4, minmax(100px, 1fr));
  column-gap: ${({ theme }) => theme.spacing(2)};
`;
