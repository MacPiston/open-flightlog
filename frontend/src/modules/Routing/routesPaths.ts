const rootPaths = {
  LANDING: "/",
  REGISTER: "/register",
  ACTIVATE: "/activate/:activationToken",
  HOME: "/home",
  FLIGHTS: "/flights",
  AIRCRAFTS: "/aircrafts",
} as const;

const aircraftsPaths = {
  MAIN: rootPaths.AIRCRAFTS,
  AIRCRAFTS: "/",
  NEW: "/new",
  AIRCRAFT: "/:aircraftId",
};

const flightsPaths = {
  MAIN: rootPaths.FLIGHTS,
  LIST: "/",
  FLIGHT: "/:flightId",
  DRAFTS_LIST: "/drafts/",
  DRAFT: "/drafts/:draftId",
};

const routesPaths = {
  ...rootPaths,
  aircraftsPaths,
  flightsPaths,
};

export default routesPaths;
