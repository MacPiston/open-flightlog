import React, { FunctionComponent, Suspense, useEffect } from "react";
import { Navigate } from "react-router-dom";

import routesPaths from "@src/modules/Routing/routesPaths";
import useAppContext from "@src/modules/App/hooks/useAppContext";
import FallbackSpinner from "@src/modules/Shared/components/FallbackSpinner";
import { RouteProps } from "@src/modules/Routing/RouteTypes/types";

const RequirePublic: FunctionComponent<RouteProps> = ({ element, title }) => {
  const hasToken = !!useAppContext().userToken;

  useEffect(() => {
    document.title = title ?? "Offblocks";
  }, [title]);

  if (hasToken) {
    return <Navigate to={routesPaths.HOME} />;
  }

  return <Suspense fallback={<FallbackSpinner />}>{element}</Suspense>;
};

export default RequirePublic;
