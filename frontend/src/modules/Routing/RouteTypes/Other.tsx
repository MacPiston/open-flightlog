import React, { FunctionComponent, Suspense, useEffect } from "react";

import FallbackSpinner from "@src/modules/Shared/components/FallbackSpinner";
import { RouteProps } from "@src/modules/Routing/RouteTypes/types";

const Other: FunctionComponent<RouteProps> = ({ element, title }) => {
  useEffect(() => {
    document.title = title ?? "Offblocks";
  }, [title]);

  return <Suspense fallback={<FallbackSpinner />}>{element}</Suspense>;
};
export default Other;
