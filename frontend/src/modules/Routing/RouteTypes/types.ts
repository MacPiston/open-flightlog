export interface RouteProps {
  element: JSX.Element;
  title?: string;
}
