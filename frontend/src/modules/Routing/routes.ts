import { RouteProps } from "react-router-dom";

import routesPaths from "@src/modules/Routing/routesPaths";

export interface RouteConfig {
  path: string;
  title: string;
  type: RouteType;
  index?: boolean;
  importComponent: () => Promise<{
    default: React.ComponentType<RouteProps> | React.ComponentType<unknown>;
  }>;
  isNested?: boolean;
}

type RouteType = "authed" | "other" | "public";

export const publicRoutes: RouteConfig[] = [
  {
    path: routesPaths.LANDING,
    title: "Welcome to Offblocks",
    type: "public",
    importComponent: () => import("@src/modules/Landing"),
  },
  {
    path: routesPaths.REGISTER,
    title: "Register | Offblocks",
    type: "public",
    importComponent: () => import("@src/modules/Register"),
  },
  {
    path: routesPaths.ACTIVATE,
    title: "Activating... | Offblocks",
    type: "public",
    importComponent: () => import("@src/modules/Register/Activate"),
  },
];

export const authedRoutes: RouteConfig[] = [
  {
    path: routesPaths.HOME,
    title: "Home | Offblocks",
    type: "authed",
    importComponent: () => import("@src/modules/Home"),
  },
  {
    path: routesPaths.flightsPaths.MAIN,
    title: "Flights | Offblocks",
    type: "authed",
    importComponent: () => import("@src/modules/Flights"),
    isNested: true,
  },
  {
    path: routesPaths.aircraftsPaths.MAIN,
    title: "Aircrafts | Offblocks",
    type: "authed",
    importComponent: () => import("@src/modules/Aircrafts"),
    isNested: true,
  },
];

const routes = [...publicRoutes, ...authedRoutes];

export default routes;
